MealAdmin.factory('Data', function($http) {
    return {
        get: function(resource, success) {
            $http.get(resource).success(function(data) {
                success(data);
            });
        },

        post: function(resource, data, success) {
            $http.post(resource, data).success(function(data) {
                success(data);
            });
        }
    };
});