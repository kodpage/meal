var MealAdmin = angular.module('MealAdmin', ['ui.router', 'satellizer', 'ui.tinymce']);

MealAdmin.config(function($stateProvider, $urlRouterProvider, $authProvider) {

    // libs
    moment.locale('tr');
    toastr.options.positionClass = 'toast-top-full-width';

    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from
    $authProvider.loginUrl = '/authenticate';

    $stateProvider.state('admin', {
        url: '/',
        templateUrl: 'scripts/views/admin.html',
        controller: 'AdminCtrl'
    });

    $stateProvider.state('auth', {
        url: '/auth',
        templateUrl: 'scripts/views/auth.html',
        controller: 'AuthCtrl'
    });

    // Redirect to the home state if any other states
    // are requested other than users
    $urlRouterProvider.otherwise('/');
});

// Token interceptor
MealAdmin.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($rootScope, $q) {
        return {
            'request': function(config) {
                config.headers = config.headers || {};

                config.headers.api_token = window.btoa('mealapi-user');

                return config;
            },

            'responseError': function(response) {
                if (response.status === 500 || response.status === 400 || response.status === 401 || response.status === 403) {
                    $rootScope.$broadcast('event:auth-error');
                }

                return $q.reject(response);
            }
        };
    });
});

MealAdmin.run(function($rootScope, $state, $auth) {
    $rootScope.$on('$stateChangeStart', function(e, to) {
        if (to.name == 'admin' && !$auth.getToken()) {
            e.preventDefault();

            $state.go('auth', {});
        }
    });
});