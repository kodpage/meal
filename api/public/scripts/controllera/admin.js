MealAdmin.controller('AdminCtrl', function($scope, $rootScope, $auth, $state, $timeout, Data) {
    $scope.sureler = [];
    $scope.sure = {};
    $scope.ayetler = [];
    $scope.aciklamalar = [];

    var $mainSecment = $('.main-segment').dimmer({
        variation: 'inverted',
        closable: false
    });

    $mainSecment.dimmer('show');
    Data.get('api/v1/sureler', function(sureler) {
        $scope.sureler = sureler;
        $scope.sure = sureler[0];

        Data.get('api/v1/ayetler/' + $scope.sure.id, function(ayetler) {
            $scope.ayetler = ayetler;

            $mainSecment.dimmer('hide');
        });
    });

    $scope.saveSure = function() {
        $mainSecment.dimmer('show');
        Data.post('admin/update-sure', $scope.sure, function() {
            $mainSecment.dimmer('hide');
            toastr.success('Sure başarıyla güncellendi.');
        });
    };
    $scope.updateAyet = function(ayet) {
        $scope.ayet = ayet;

        $('.update-ayet-modal').modal({
            autofocus: true,
            closable: false,
            onApprove: function() {
                if (!$scope.ayet.ayet.trim()) {
                    toastr.error('Ayet metni boş olamaz!');

                    return false;
                } else {
                    $mainSecment.dimmer('show');

                    Data.post('admin/update-ayet', $scope.ayet, function() {
                        $mainSecment.dimmer('hide');

                        toastr.success('Ayet başarıyla güncellendi.');
                        $scope.ayet.updated_at = moment();
                    });
                }
            }
        }).modal('show');
    };
    $scope.updateAciklamalar = function(ayet) {
        $scope.ayet = ayet;

        Data.get('api/v1/aciklamalar/' + $scope.ayet.id, function(aciklamalar) {
            $scope.aciklamalar = aciklamalar;

            $timeout(function() {
                $scope.aciklamalarModal = $('.update-aciklamalar-modal').modal({
                    closable: false,
                    onApprove: function() {
                        var data = {},
                            errorMsg = '';

                        for (var i = 0; i < $scope.aciklamalar.length; i++) {
                            var a = $scope.aciklamalar[i];

                            if (!a.pozisyon) {
                                errorMsg = (i + 1) +'. açıklamanın pozisyonu girilmedi!';

                                break;
                            }

                            if (!_.isString(a.aciklama) || !a.aciklama.trim()) {
                                errorMsg = (i + 1) +'. açıklamanın metni girilmedi!';

                                break;
                            }
                        }

                        if (errorMsg) {
                            toastr.error(errorMsg);
                            return false;
                        } else {
                            data['aciklamalar'] = $scope.aciklamalar;
                            data['ayet_id'] = $scope.ayet.id;

                            $mainSecment.dimmer('show');

                            Data.post('admin/update-aciklamalar', data, function() {
                                $mainSecment.dimmer('hide');

                                toastr.success('Ayet açıklamaları başarıyla güncellendi.');
                            });
                        }
                    }
                }).modal('show').modal('refresh');
            })
        });
    };
    $scope.addAciklama = function() {
        $scope.aciklamalar.push({
            aciklama: '',
            pozisyon: null,
            id: _.uniqueId('aciklama')
        });

        $timeout(function() {
            $scope.aciklamalarModal.modal('refresh');
        }, 50);
    };
    $scope.removeAciklama = function(aciklama) {
        $scope.aciklamalar = $scope.aciklamalar.filter(function(a) {
            return a.id != aciklama.id;
        });

        $timeout(function() {
            $scope.aciklamalarModal.modal('refresh');
        }, 50);
    };


    $('.sureler-select').dropdown({
        onChange: function(value, text, $choice) {
            $scope.$apply(function() {
                $scope.sure = $choice.data('sure');

                $mainSecment.dimmer('show');
                Data.get('api/v1/ayetler/' + $scope.sure.id, function(ayetler) {
                    $scope.ayetler = ayetler;

                    $mainSecment.dimmer('hide');
                });
            });
        }
    });
    $('.tabular.menu .item').tab();
    $scope.tinymceOptions = {
        content_css: '/assets/css/tinymce.css',
    };
    $scope.formatDate = function(date) {
        return moment(date).format('DD-MM-YYYY HH:mm:ss');
    };
    $rootScope.$on('event:auth-error', function() {
        toastr.error('Oturum hatası oluştuğundan dolayı giriş sayfasına yönlendiriliyorsunuz!');
        $state.go('auth', {});
    });

    $scope.logout = function() {
        $auth.logout();
        $state.go('auth', {});
    };
});