MealAdmin.controller('AuthCtrl', function($scope, $auth, $state) {
    $('.auth-form').form({
        inline: true,
        fields: {
            email: {
                identifier  : 'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'E-posta adresi boş olamaz.'
                    },
                    {
                        type   : 'email',
                        prompt : 'Geçerli bir e-posta adresi giriniz.'
                    }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Parola boş olamaz.'
                    },
                    {
                        type   : 'length[4]',
                        prompt : 'Parola en az 4 karekter olmalıdır.'
                    }
                ]
            }
        },

        onSuccess: function(e) {
            e.preventDefault();

            var credentials = {
                email: $scope.email,
                password: $scope.password
            };

            // Use Satellizer's $auth service to login
            $auth.login(credentials).then(function(data) {
                $state.go('admin', {});
            }).catch(function(response) {
                toastr.error('Hatalı e-posta adresi veya parola!')
            });

            return false;
        }
    });

});