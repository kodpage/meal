<?php

$xml = simplexml_load_file(dirname(__FILE__) . '/quran.xml');
$js = 'var quranArabic = {';

foreach($xml as $sure) {
    foreach ($sure as $ayet) {
        $js .= '"' . $sure['index'] . '-' . $ayet['index'] . '": "' . $ayet['text'] .'",';
    }
}
$js .= '};';

file_put_contents(dirname(__FILE__) . '/quran.js', $js);