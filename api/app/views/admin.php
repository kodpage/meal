<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Hayat Kitabı Kuran Yönetim Paneli</title>


        <link rel="stylesheet" href="/assets/vendor/semantic/dist/semantic.min.css">
        <link rel="stylesheet" href="/assets/vendor/toastr/toastr.min.css">
        <link rel="stylesheet" href="/assets/css/styles.css">

        <!--[if lte IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/Base64/0.3.0/base64.min.js"></script>
        <![endif]-->

        <script src="/assets/vendor/moment/min/moment-with-locales.min.js"></script>
        <script src="/assets/vendor/underscore/underscore-min.js"></script>
        <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="/assets/vendor/tinymce-dist/tinymce.jquery.min.js"></script>
        <script src="/assets/vendor/angular/angular.min.js"></script>
        <script src="/assets/vendor/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script src="/assets/vendor/satellizer/satellizer.min.js"></script>
        <script src="/assets/vendor/angular-ui-tinymce/src/tinymce.js"></script>
        <script src="/assets/vendor/semantic/dist/semantic.min.js"></script>
        <script src="/assets/vendor/toastr/toastr.min.js"></script>
        <script src="/assets/js/tinymce/tr.js"></script>

        <!-- App Bootstrap -->
        <script src="/scripts/app.js"></script>

        <!-- App Controllers -->
        <script src="/scripts/controllera/auth.js"></script>
        <script src="/scripts/controllera/admin.js"></script>

        <!-- App Services -->
        <script src="scripts/services/data.js"></script>
    </head>
    <body ng-app="MealAdmin" ui-view>
    </body>
</html>