<?php

class MealAdminController extends BaseController {

    public function postUpdateSure()
    {
        $aciklama = Input::get('aciklamasi');
        $video = Input::get('vidyo');
        $id = Input::get('id');

        $sure = Sure::find($id);
        $sure->aciklamasi = $aciklama;
        $sure->vidyo = $video;
        $sure->save();

        return Response::json(['status'=>'success'], 200);
    }

    public function postUpdateAyet()
    {
        $ayetText = Input::get('ayet');
        $ses = Input::get('ses');
        $id = Input::get('id');

        $ayet = Ayet::find($id);
        $ayet->ayet = $ayetText;
        $ayet->ses = $ses;
        $ayet->save();

        return Response::json(['status'=>'success'], 200);
    }

    public function postUpdateAciklamalar()
    {
        $aciklamalar = Input::get('aciklamalar');
        $ayetId = Input::get('ayet_id');
        $indexStart = 1;

        $lastAciklama = Aciklama::where('ayet_id', $ayetId)->orderBy('aciklama_index', 'desc')->first();
        if ($lastAciklama) {
            $indexStart = intval($lastAciklama->aciklama_index) + 1;
        }

        // Remove existing aciklamalar.
        Aciklama::where('ayet_id', $ayetId)->delete();

        foreach($aciklamalar as $a) {
            $aciklama = Aciklama::find($a['id']);

            if (! $aciklama) {
                $aciklama = new Aciklama();
            }

            $aciklama->ayet_id = $ayetId;
            $aciklama->aciklama = $a['aciklama'];
            $aciklama->pozisyon = $a['pozisyon'];
            $aciklama->aciklama_index = $indexStart;
            $aciklama->save();

            $indexStart++;
        }
    }
}