<?php

class MealApiController extends BaseController {

    public function getEverything($sureId = null)
    {
        $data = [];

        if ($sureId) {
            $data['ayetler'] = Sure::find($sureId)->ayetler;
            $data['aciklamalar'] = [];

            foreach ($data['ayetler'] as $ayet) {
                foreach ($ayet->aciklamalar as $aciklama) {
                    $data['aciklamalar'][] = $aciklama;
                }

                unset($ayet['aciklamalar']);
            }

            $data['ayetler'] = $data['ayetler']->toArray();

            return Response::json($data, 200);
        } else {
            $data['sureler'] = [];
            $data['ayetler'] = [];
            $data['aciklamalar'] = [];

            foreach (Sure::all() as $sure) {
                $ayetler = $sure->ayetler;

                foreach ($ayetler as $ayet) {
                    foreach ($ayet->aciklamalar as $aciklama) {
                        $data['aciklamalar'][] = $aciklama->toArray();
                    }

                    unset($ayet['aciklamalar']);

                    $data['ayetler'][] = $ayet->toArray();
                }

                unset($sure['ayetler']);

                $data['sureler'][] = $sure->toArray();
            }

            return Response::json($data, 200);
        }
    }

    public function getSureler()
    {
        return Response::json(Sure::all(), 200);
    }

    public function getAyetler($sureId)
    {
        return Response::json(Sure::find($sureId)->ayetler, 200);
    }

    public function getAciklamalar($ayetId)
    {
        return Response::json(Ayet::find($ayetId)->aciklamalar, 200);
    }

    public function postSearch()
    {
        $query = Input::get('query');
        $type = Input::get('type');
        $cacheSearchKey = 'cache-search-key-' . $query . '-' . $type;

        if (Cache::has($cacheSearchKey)) {
            $data = Cache::get($cacheSearchKey);
        } else {
            $data = [
                'query' => $query,
                'type' => $type,
                'results' => [],
                'result_count' => 0
            ];

            $query = '*' . $query . '*';

            if ($type == 'all' || $type == 'sure') {
                $sureler = Sure::whereRaw('MATCH(ismi_cleaned) AGAINST(? IN BOOLEAN MODE)', [$query])->get();
            }

            if ($type == 'all' || $type == 'ayet') {
                $ayetler = Ayet::whereRaw('MATCH(ayet) AGAINST(? IN BOOLEAN MODE)', [$query])->get();

                // Lazy-load
                $ayetler->load('sure');
            }

            if ($type == 'all' || $type == 'aciklama') {
                $aciklamalar = Aciklama::whereRaw('MATCH(aciklama) AGAINST(? IN BOOLEAN MODE)', [$query])->get();

                // Lazy-load
                $aciklamalar->load('ayet');
            }

            // Sureler
            if ($type == 'all' || $type == 'sure') {
                if ($sureler->count()) {
                    foreach ($sureler as $s) {
                        array_push($data['results'], [
                            'type' => 'sure',
                            'text' => $s->ismi,
                            'extra' => [
                                'id' => $s->id,
                                'sirasi' => $s->sirasi,
                                'inis_sirasi' => $s->inis_sirasi,
                                'ayet_sayisi' => $s->ayet_sayisi
                            ]
                        ]);

                        $data['result_count']++;
                    }
                }
            }

            // Ayetler
            if ($type == 'all' || $type == 'ayet') {
                if ($ayetler->count() && $data['result_count'] <= 300) {
                    foreach ($ayetler as $a) {
                        array_push($data['results'], [
                            'type' => 'ayet',
                            'text' => $a->ayet,
                            'extra' => [
                                'ayet_id' => $a->id,
                                'ayet_index' => $a->ayet_index,
                                'sure_ismi' => $a->sure->ismi,
                                'sure_id' => $a->sure_id,
                                'sure_sirasi' => $a->sure->sirasi,
                                'sure_inis_sirasi' => $a->sure->inis_sirasi,
                                'sure_ayet_sayisi' => $a->sure->ayet_sayisi
                            ]
                        ]);

                        if ($data['result_count'] > 300) {
                            break;
                        } else {
                            $data['result_count']++;
                        }
                    }
                }
            }

            // Aciklamar
            if ($type == 'all' || $type == 'aciklama') {
                if ($aciklamalar->count() && $data['result_count'] <= 300) {
                    foreach ($aciklamalar as $d) {
                        array_push($data['results'], [
                            'type' => 'aciklama',
                            'text' => $d->aciklama,
                            'extra' => [
                                'sure_ismi' => $d->ayet->sure->ismi,
                                'sure_id' => $d->ayet->sure_id,
                                'sure_sirasi' => $d->ayet->sure->sirasi,
                                'sure_inis_sirasi' => $d->ayet->sure->inis_sirasi,
                                'sure_ayet_sayisi' => $d->ayet->sure->ayet_sayisi,
                                'ayet_id' => $d->ayet_id,
                                'ayet_ayet' => $d->ayet->ayet,
                                'ayet_index' => $d->ayet->ayet_index,
                                'aciklama_index' => $d->aciklama_index
                            ]
                        ]);

                        if ($data['result_count'] > 300) {
                            break;
                        } else {
                            $data['result_count']++;
                        }
                    }
                }
            }

            Cache::forever($cacheSearchKey, $data);
        }

        return Response::json($data, 200);
    }
}