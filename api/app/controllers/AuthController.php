<?php

class AuthController extends BaseController
{
    public function authenticate()
    {
        $credentials = Request::only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return Response::json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return Response::json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return Response::json(compact('token'));
    }

}
