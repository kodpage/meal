<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Admin routes
use Tymon\JWTAuth\Exceptions\JWTException;

Route::get('/', function () {
    return View::make('admin');
});

// Authenticate url
Route::post('authenticate', 'AuthController@authenticate');

// Admin routes
Route::group(['prefix' => 'admin', 'before' => 'jwt-auth'], function () {
    Route::controller('/', 'MealAdminController');
});

// Api routes
Route::group(['prefix' => 'api/v1', 'before' => 'api-auth'], function () {
    Route::controller('/', 'MealApiController');
});