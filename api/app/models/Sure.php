<?php

class Sure extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sureler';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function ayetler()
    {
        return $this->hasMany('Ayet', 'sure_id');
    }
}