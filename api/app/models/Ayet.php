<?php

class Ayet extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ayetler';

    public function sure()
    {
        return $this->belongsTo('Sure', 'sure_id');
    }

    public function aciklamalar()
    {
        return $this->hasMany('Aciklama', 'ayet_id');
    }
}