<?php

class Aciklama extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aciklamalar';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function ayet()
    {
        return $this->belongsTo('Ayet', 'ayet_id');
    }
}