<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurelerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sureler', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ismi');
            $table->string('ismi_cleaned');
            $table->text('aciklamasi');
            $table->text('vidyo')->nullable();
            $table->integer('ayet_sayisi');
            $table->integer('sirasi');
            $table->integer('inis_sirasi');

            $table->engine = 'MyISAM';
        });

        DB::statement('ALTER TABLE sureler ADD FULLTEXT search(ismi_cleaned)');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sureler');
    }

}
