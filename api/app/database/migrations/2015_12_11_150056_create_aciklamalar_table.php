<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAciklamalarTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aciklamalar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ayet_id')->unsigned();
            $table->integer('aciklama_index')->unsigned();
            $table->integer('pozisyon')->nullable()->unsigned();
            $table->text('aciklama');

            $table->engine = 'MyISAM';
        });

        DB::statement('ALTER TABLE aciklamalar ADD FULLTEXT search(aciklama)');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aciklamalar');
    }

}
