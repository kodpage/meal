<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAyetlerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayetler', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sure_id')->unsigned();
            $table->integer('ayet_index')->unsigned();
            $table->text('ayet');
            $table->text('ses')->nullable();
            $table->timestamps();

            $table->engine = 'MyISAM';
        });

        DB::statement('ALTER TABLE ayetler ADD FULLTEXT search(ayet)');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ayetler');
    }

}
