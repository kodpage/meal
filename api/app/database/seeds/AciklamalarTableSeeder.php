<?php

class AciklamalarTableSeeder extends Seeder
{

    public function run()
    {
        $ayetler = Ayet::all();
        $faker = Faker\Factory::create();

        DB::table('aciklamalar')->truncate();

        foreach ($ayetler as $a) {
            for ($i = 1; $i <= $faker->numberBetween(1, 4); $i++) {
                if ($faker->numberBetween(1, 300) == 5) {
                    $aciklamaText = $faker->paragraph($faker->numberBetween(1, 10)) . ' demo';
                } else {
                    $aciklamaText = $faker->paragraph($faker->numberBetween(1, 10));
                }

                $aciklama = new Aciklama();
                $aciklama->ayet_id = $a->id;
                $aciklama->aciklama_index = $i;
                $aciklama->aciklama = $aciklamaText;
                $aciklama->save();
            }
        }
    }

}