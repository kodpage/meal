<?php

class AyetlerTableSeeder extends Seeder
{

    public function run()
    {
        $sureler = Sure::all();
        $faker = Faker\Factory::create();

        DB::table('ayetler')->truncate();

        foreach ($sureler as $s) {
            for ($i = 1; $i <= $s->ayet_sayisi; $i++) {
                if ($faker->numberBetween(1, 250) == 5) {
                    $ayetText = $faker->paragraph($faker->numberBetween(1, 4)) . ' demo';
                } else {
                    $ayetText = $faker->paragraph($faker->numberBetween(1, 4));
                }

                $ayet = new Ayet();
                $ayet->ayet = $ayetText;
                $ayet->ayet_index = $i;
                $ayet->sure_id = $s->id;
                $ayet->save();
            }
        }
    }

}