<?php

class SurelerTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('sureler')->truncate();

        $sureler = [
            [
                "ismi" => "Fatiha",
                "ayetSayisi" => 7,
                "inisSirasi" => 5,
                "sirasi" => 1
            ],
            [
                "ismi" => "Bakara",
                "ayetSayisi" => 286,
                "inisSirasi" => 87,
                "sirasi" => 2
            ],

            [
                "ismi" => "Âl-i İmrân",
                "ayetSayisi" => 200,
                "inisSirasi" => 89,
                "sirasi" => 3
            ],
            [
                "ismi" => "Nisa",
                "ayetSayisi" => 176,
                "inisSirasi" => 92,
                "sirasi" => 4
            ],
            [
                "ismi" => "Maide",
                "ayetSayisi" => 120,
                "inisSirasi" => 112,
                "sirasi" => 5
            ],
            [
                "ismi" => "En’âm",
                "ayetSayisi" => 165,
                "inisSirasi" => 55,
                "sirasi" => 6
            ],
            [
                "ismi" => "A'raf",
                "ayetSayisi" => 206,
                "inisSirasi" => 39,
                "sirasi" => 7
            ],
            [
                "ismi" => "Enfal",
                "ayetSayisi" => 75,
                "inisSirasi" => 88,
                "sirasi" => 8
            ],
            [
                "ismi" => "Tevbe",
                "ayetSayisi" => 129,
                "inisSirasi" => 113,
                "sirasi" => 9
            ],
            [
                "ismi" => "Yunus",
                "ayetSayisi" => 109,
                "inisSirasi" => 51,
                "sirasi" => 10
            ],
            [
                "ismi" => "Hud",
                "ayetSayisi" => 123,
                "inisSirasi" => 52,
                "sirasi" => 11
            ],
            [
                "ismi" => "Yusuf",
                "ayetSayisi" => 111,
                "inisSirasi" => 53,
                "sirasi" => 12
            ],
            [
                "ismi" => "Ra'd",
                "ayetSayisi" => 43,
                "inisSirasi" => 96,
                "sirasi" => 13
            ],
            [
                "ismi" => "İbrahim",
                "ayetSayisi" => 52,
                "inisSirasi" => 72,
                "sirasi" => 14
            ],
            [
                "ismi" => "Hicr",
                "ayetSayisi" => 99,
                "inisSirasi" => 54,
                "sirasi" => 15
            ],
            [
                "ismi" => "Nahl",
                "ayetSayisi" => 128,
                "inisSirasi" => 70,
                "sirasi" => 16
            ],
            [
                "ismi" => "İsra",
                "ayetSayisi" => 111,
                "inisSirasi" => 50,
                "sirasi" => 17
            ],
            [
                "ismi" => "Kehf",
                "ayetSayisi" => 110,
                "inisSirasi" => 69,
                "sirasi" => 18
            ],
            [
                "ismi" => "Meryem",
                "ayetSayisi" => 98,
                "inisSirasi" => 44,
                "sirasi" => 19
            ],
            [
                "ismi" => "Ta-Ha",
                "ayetSayisi" => 135,
                "inisSirasi" => 45,
                "sirasi" => 20
            ],
            [
                "ismi" => "Enbiya",
                "ayetSayisi" => 112,
                "inisSirasi" => 73,
                "sirasi" => 21
            ],
            [
                "ismi" => "Hac",
                "ayetSayisi" => 78,
                "inisSirasi" => 103,
                "sirasi" => 22
            ],
            [
                "ismi" => "Mü'minun",
                "ayetSayisi" => 118,
                "inisSirasi" => 74,
                "sirasi" => 23
            ],
            [
                "ismi" => "Nur",
                "ayetSayisi" => 64,
                "inisSirasi" => 102,
                "sirasi" => 24
            ],
            [
                "ismi" => "Furkan",
                "ayetSayisi" => 77,
                "inisSirasi" => 42,
                "sirasi" => 25
            ],
            [
                "ismi" => "Şu'ara",
                "ayetSayisi" => 227,
                "inisSirasi" => 47,
                "sirasi" => 26
            ],
            [
                "ismi" => "Neml",
                "ayetSayisi" => 93,
                "inisSirasi" => 48,
                "sirasi" => 27
            ],
            [
                "ismi" => "Kasas",
                "ayetSayisi" => 88,
                "inisSirasi" => 49,
                "sirasi" => 28
            ],
            [
                "ismi" => "Ankebut",
                "ayetSayisi" => 69,
                "inisSirasi" => 85,
                "sirasi" => 29
            ],
            [
                "ismi" => "Rum",
                "ayetSayisi" => 60,
                "inisSirasi" => 84,
                "sirasi" => 30
            ],
            [
                "ismi" => "Lokman",
                "ayetSayisi" => 34,
                "inisSirasi" => 57,
                "sirasi" => 31
            ],
            [
                "ismi" => "Secde",
                "ayetSayisi" => 30,
                "inisSirasi" => 75,
                "sirasi" => 32
            ],
            [
                "ismi" => "Ahzab",
                "ayetSayisi" => 73,
                "inisSirasi" => 90,
                "sirasi" => 33
            ],
            [
                "ismi" => "Sebe'",
                "ayetSayisi" => 54,
                "inisSirasi" => 58,
                "sirasi" => 34
            ],
            [
                "ismi" => "Fatır",
                "ayetSayisi" => 45,
                "inisSirasi" => 43,
                "sirasi" => 35
            ],
            [
                "ismi" => "Yasin",
                "ayetSayisi" => 83,
                "inisSirasi" => 41,
                "sirasi" => 36
            ],
            [
                "ismi" => "Saffat",
                "ayetSayisi" => 182,
                "inisSirasi" => 56,
                "sirasi" => 37
            ],
            [
                "ismi" => "Sad",
                "ayetSayisi" => 88,
                "inisSirasi" => 38,
                "sirasi" => 38
            ],
            [
                "ismi" => "Zümer",
                "ayetSayisi" => 75,
                "inisSirasi" => 59,
                "sirasi" => 39
            ],
            [
                "ismi" => "Mü'min",
                "ayetSayisi" => 85,
                "inisSirasi" => 60,
                "sirasi" => 40
            ],
            [
                "ismi" => "Fussilet",
                "ayetSayisi" => 54,
                "inisSirasi" => 61,
                "sirasi" => 41
            ],
            [
                "ismi" => "Şura",
                "ayetSayisi" => 53,
                "inisSirasi" => 62,
                "sirasi" => 42
            ],
            [
                "ismi" => "Zuhruf",
                "ayetSayisi" => 89,
                "inisSirasi" => 63,
                "sirasi" => 43
            ],
            [
                "ismi" => "Duhan",
                "ayetSayisi" => 59,
                "inisSirasi" => 64,
                "sirasi" => 44
            ],
            [
                "ismi" => "Casiye",
                "ayetSayisi" => 37,
                "inisSirasi" => 65,
                "sirasi" => 45
            ],
            [
                "ismi" => "Ahkaf",
                "ayetSayisi" => 35,
                "inisSirasi" => 66,
                "sirasi" => 46
            ],
            [
                "ismi" => "Muhammed",
                "ayetSayisi" => 38,
                "inisSirasi" => 95,
                "sirasi" => 47
            ],
            [
                "ismi" => "Fetih",
                "ayetSayisi" => 29,
                "inisSirasi" => 111,
                "sirasi" => 48
            ],
            [
                "ismi" => "Hucurat",
                "ayetSayisi" => 18,
                "inisSirasi" => 106,
                "sirasi" => 49
            ],
            [
                "ismi" => "Kaf",
                "ayetSayisi" => 45,
                "inisSirasi" => 34,
                "sirasi" => 50
            ],
            [
                "ismi" => "Zariyat",
                "ayetSayisi" => 60,
                "inisSirasi" => 67,
                "sirasi" => 51
            ],
            [
                "ismi" => "Tur",
                "ayetSayisi" => 49,
                "inisSirasi" => 76,
                "sirasi" => 52
            ],
            [
                "ismi" => "Necm",
                "ayetSayisi" => 62,
                "inisSirasi" => 23,
                "sirasi" => 53
            ],
            [
                "ismi" => "Kamer",
                "ayetSayisi" => 55,
                "inisSirasi" => 37,
                "sirasi" => 54
            ],
            [
                "ismi" => "Rahman",
                "ayetSayisi" => 78,
                "inisSirasi" => 97,
                "sirasi" => 55
            ],
            [
                "ismi" => "Vakı'a",
                "ayetSayisi" => 96,
                "inisSirasi" => 46,
                "sirasi" => 56
            ],
            [
                "ismi" => "Hadid",
                "ayetSayisi" => 29,
                "inisSirasi" => 94,
                "sirasi" => 57
            ],
            [
                "ismi" => "Mücadele",
                "ayetSayisi" => 22,
                "inisSirasi" => 105,
                "sirasi" => 58
            ],
            [
                "ismi" => "Haşr",
                "ayetSayisi" => 24,
                "inisSirasi" => 101,
                "sirasi" => 59
            ],
            [
                "ismi" => "Mümtehine",
                "ayetSayisi" => 13,
                "inisSirasi" => 91,
                "sirasi" => 60
            ],
            [
                "ismi" => "Saf",
                "ayetSayisi" => 14,
                "inisSirasi" => 109,
                "sirasi" => 61
            ],
            [
                "ismi" => "Cum'a",
                "ayetSayisi" => 11,
                "inisSirasi" => 110,
                "sirasi" => 62
            ],
            [
                "ismi" => "Munafikun",
                "ayetSayisi" => 11,
                "inisSirasi" => 104,
                "sirasi" => 63
            ],
            [
                "ismi" => "Teğabun",
                "ayetSayisi" => 18,
                "inisSirasi" => 108,
                "sirasi" => 64
            ],
            [
                "ismi" => "Talak",
                "ayetSayisi" => 12,
                "inisSirasi" => 99,
                "sirasi" => 65
            ],
            [
                "ismi" => "Tahrim",
                "ayetSayisi" => 12,
                "inisSirasi" => 107,
                "sirasi" => 66
            ],
            [
                "ismi" => "Mülk",
                "ayetSayisi" => 30,
                "inisSirasi" => 77,
                "sirasi" => 67
            ],
            [
                "ismi" => "Kalem",
                "ayetSayisi" => 52,
                "inisSirasi" => 2,
                "sirasi" => 68
            ],
            [
                "ismi" => "Hakka",
                "ayetSayisi" => 52,
                "inisSirasi" => 78,
                "sirasi" => 69
            ],
            [
                "ismi" => "Me'aric",
                "ayetSayisi" => 44,
                "inisSirasi" => 79,
                "sirasi" => 70
            ],
            [
                "ismi" => "Nuh",
                "ayetSayisi" => 28,
                "inisSirasi" => 71,
                "sirasi" => 71
            ],
            [
                "ismi" => "Cin",
                "ayetSayisi" => 28,
                "inisSirasi" => 40,
                "sirasi" => 72
            ],
            [
                "ismi" => "Müzzemmil",
                "ayetSayisi" => 20,
                "inisSirasi" => 3,
                "sirasi" => 73
            ],
            [
                "ismi" => "Müddesir",
                "ayetSayisi" => 56,
                "inisSirasi" => 4,
                "sirasi" => 74
            ],
            [
                "ismi" => "Kıyamet",
                "ayetSayisi" => 40,
                "inisSirasi" => 31,
                "sirasi" => 75
            ],
            [
                "ismi" => "İnsan",
                "ayetSayisi" => 31,
                "inisSirasi" => 98,
                "sirasi" => 76
            ],
            [
                "ismi" => "Mürselat",
                "ayetSayisi" => 50,
                "inisSirasi" => 33,
                "sirasi" => 77
            ],
            [
                "ismi" => "Nebe'",
                "ayetSayisi" => 40,
                "inisSirasi" => 80,
                "sirasi" => 78
            ],
            [
                "ismi" => "Nazi'at",
                "ayetSayisi" => 46,
                "inisSirasi" => 81,
                "sirasi" => 79
            ],
            [
                "ismi" => "Abese",
                "ayetSayisi" => 42,
                "inisSirasi" => 24,
                "sirasi" => 80
            ],
            [
                "ismi" => "Tekvir",
                "ayetSayisi" => 29,
                "inisSirasi" => 7,
                "sirasi" => 81
            ],
            [
                "ismi" => "İnfitar",
                "ayetSayisi" => 19,
                "inisSirasi" => 82,
                "sirasi" => 82
            ],
            [
                "ismi" => "Mutaffifin",
                "ayetSayisi" => 36,
                "inisSirasi" => 86,
                "sirasi" => 83
            ],
            [
                "ismi" => "İnşikak",
                "ayetSayisi" => 25,
                "inisSirasi" => 83,
                "sirasi" => 84
            ],
            [
                "ismi" => "Büruc",
                "ayetSayisi" => 22,
                "inisSirasi" => 27,
                "sirasi" => 85
            ],
            [
                "ismi" => "Tarık",
                "ayetSayisi" => 17,
                "inisSirasi" => 36,
                "sirasi" => 86
            ],
            [
                "ismi" => "A'la",
                "ayetSayisi" => 19,
                "inisSirasi" => 8,
                "sirasi" => 87
            ],
            [
                "ismi" => "Gaşiye",
                "ayetSayisi" => 26,
                "inisSirasi" => 68,
                "sirasi" => 88
            ],
            [
                "ismi" => "Fecr",
                "ayetSayisi" => 30,
                "inisSirasi" => 10,
                "sirasi" => 89
            ],
            [
                "ismi" => "Beled",
                "ayetSayisi" => 20,
                "inisSirasi" => 35,
                "sirasi" => 90
            ],
            [
                "ismi" => "Şems",
                "ayetSayisi" => 15,
                "inisSirasi" => 26,
                "sirasi" => 91
            ],
            [
                "ismi" => "Leyl",
                "ayetSayisi" => 21,
                "inisSirasi" => 9,
                "sirasi" => 92
            ],
            [
                "ismi" => "Duha",
                "ayetSayisi" => 11,
                "inisSirasi" => 11,
                "sirasi" => 93
            ],
            [
                "ismi" => "İnşirah",
                "ayetSayisi" => 8,
                "inisSirasi" => 12,
                "sirasi" => 94
            ],
            [
                "ismi" => "Tin",
                "ayetSayisi" => 8,
                "inisSirasi" => 28,
                "sirasi" => 95
            ],
            [
                "ismi" => "Âlak",
                "ayetSayisi" => 19,
                "inisSirasi" => 1,
                "sirasi" => 96
            ],
            [
                "ismi" => "Kadir",
                "ayetSayisi" => 5,
                "inisSirasi" => 25,
                "sirasi" => 97
            ],
            [
                "ismi" => "Beyyine",
                "ayetSayisi" => 8,
                "inisSirasi" => 100,
                "sirasi" => 98
            ],
            [
                "ismi" => "Zilzal",
                "ayetSayisi" => 8,
                "inisSirasi" => 93,
                "sirasi" => 99
            ],
            [
                "ismi" => "Adiyat",
                "ayetSayisi" => 11,
                "inisSirasi" => 14,
                "sirasi" => 100
            ],
            [
                "ismi" => "Kari'a",
                "ayetSayisi" => 11,
                "inisSirasi" => 30,
                "sirasi" => 101
            ],
            [
                "ismi" => "Tekasür",
                "ayetSayisi" => 8,
                "inisSirasi" => 16,
                "sirasi" => 102
            ],
            [
                "ismi" => "Asr",
                "ayetSayisi" => 3,
                "inisSirasi" => 13,
                "sirasi" => 103
            ],
            [
                "ismi" => "Hümeze",
                "ayetSayisi" => 9,
                "inisSirasi" => 32,
                "sirasi" => 104
            ],
            [
                "ismi" => "Fil",
                "ayetSayisi" => 5,
                "inisSirasi" => 19,
                "sirasi" => 105
            ],
            [
                "ismi" => "Kureyş",
                "ayetSayisi" => 4,
                "inisSirasi" => 29,
                "sirasi" => 106
            ],
            [
                "ismi" => "Ma'un",
                "ayetSayisi" => 7,
                "inisSirasi" => 17,
                "sirasi" => 107
            ],
            [
                "ismi" => "Kevser",
                "ayetSayisi" => 3,
                "inisSirasi" => 15,
                "sirasi" => 108
            ],
            [
                "ismi" => "Kâfirun",
                "ayetSayisi" => 6,
                "inisSirasi" => 18,
                "sirasi" => 109
            ],
            [
                "ismi" => "Nasr",
                "ayetSayisi" => 3,
                "inisSirasi" => 114,
                "sirasi" => 110
            ],
            [
                "ismi" => "Tebbet",
                "ayetSayisi" => 5,
                "inisSirasi" => 6,
                "sirasi" => 111
            ],
            [
                "ismi" => "İhlas",
                "ayetSayisi" => 4,
                "inisSirasi" => 22,
                "sirasi" => 112
            ],
            [
                "ismi" => "Felak",
                "ayetSayisi" => 5,
                "inisSirasi" => 20,
                "sirasi" => 113
            ],
            [
                "ismi" => "Nas",
                "ayetSayisi" => 6,
                "inisSirasi" => 21,
                "sirasi" => 114
            ]
        ];

        $faker = Faker\Factory::create();

        foreach($sureler as $s) {
            $cleaned = str_replace('Â', 'A', $s['ismi']);
            $cleaned = str_replace('â', 'a', $cleaned);
            $cleaned = str_replace('’', '', $cleaned);
            $cleaned = str_replace('\'', '', $cleaned);
            $cleaned = str_replace('-', '', $cleaned);

            if ($faker->numberBetween(1, 50) == 5) {
                $cleaned .= ' demo';
            }

            $aciklamasi = '';
            foreach ($faker->paragraphs($faker->numberBetween(1, 5)) as $p) {
                $aciklamasi .= $p . '\r\n';
            }

            $sure = new Sure();
            $sure->ismi = $s['ismi'];
            $sure->ismi_cleaned = $cleaned;
            $sure->aciklamasi = $aciklamasi;
            $sure->ayet_sayisi = $s['ayetSayisi'];
            $sure->inis_sirasi = $s['inisSirasi'];
            $sure->sirasi = $s['sirasi'];
            $sure->save();
        }
    }

}