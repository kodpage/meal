<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@kodpage.com',
            'password' => Hash::make('dfd123dfd'),
            'first_name' => 'System',
            'last_name' => 'Admin',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        $this->call('SurelerTableSeeder');
        $this->call('AyetlerTableSeeder');
        $this->call('AciklamalarTableSeeder');
    }

}
