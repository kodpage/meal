#!/usr/bin/env bash

# Install npm dependencies.
npm install

# Add platforms
ionic platform add ios
ionic platform add android

# Install the required plugins
cordova plugin add cordova-plugin-crosswalk-webview
cordova plugin add https://github.com/litehelpers/Cordova-sqlite-storage.git
cordova plugin add https://github.com/EddyVerbruggen/Insomnia-PhoneGap-Plugin.git
cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git
cordova plugin add cordova-instagram-plugin
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add https://github.com/Glitchbone/CordovaYoutubeVideoPlayer.git
cordova plugin add https://github.com/devgeeks/Canvas2ImagePlugin.git
cordova plugin add cordova-plugin-dialogs
cordova plugin add org.apache.cordova.media
cordova plugin add org.apache.cordova.splashscreen
cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard.git
cordova plugin add cordova-plugin-console
cordova plugin add cordova-plugin-device
cordova plugin add cordova-plugin-statusbar
cordova plugin add cordova-plugin-whitelist

# Build
ionic build ios
ionic build android