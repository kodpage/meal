Meal.factory('Ayetler', function($localStorage, $cordovaSQLite, Data, Settings) {
    if (!_.isObject($localStorage.ayetler)) {
        $localStorage.ayetler = {};
    }

    return {
        all: function(sure_id, success) {
            var ayetler = [],
                showArabic = Settings.get('showAyetArabic'),
                that = this;

            if (Settings.get('netUsageType') == 'online') {
                Data.get('ayetler/' + sure_id, function(data) {
                    ayetler = data.map(function(d) {
                        d.id = parseInt(d.id);
                        d.sure_id = parseInt(d.sure_id);
                        d.ayet_index = parseInt(d.ayet_index);

                        return d;
                    });

                    success(ayetler);
                });
            } else {
                if (_.isArray($localStorage.ayetler[sure_id]) && $localStorage.ayetler[sure_id].length > 0) {
                    success($localStorage.ayetler[sure_id]);
                } else {
                    var q = 'SELECT * FROM ayetler WHERE sure_id=?';

                    $cordovaSQLite.execute(db, q, [sure_id]).then(function(res) {
                        if (res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                ayetler.push(res.rows.item(i));
                            }

                            success(ayetler);

                            $localStorage.ayetler[sure_id] = ayetler;
                        } else {
                            Data.get('ayetler/' + sure_id, function(data) {
                                ayetler = data.map(function(d) {
                                    d.id = parseInt(d.id);
                                    d.sure_id = parseInt(d.sure_id);
                                    d.ayet_index = parseInt(d.ayet_index);

                                    return d;
                                });

                                success(ayetler);

                                $localStorage.ayetler[sure_id] = ayetler;

                                db.transaction(function(tx) {
                                    _.each(ayetler, function(d) {
                                        tx.executeSql('SELECT * FROM ayetler WHERE id=?', [d.id], function(tx, res) {
                                            if (res.rows.length < 1) {
                                                q = 'INSERT INTO ayetler (id, sure_id, ayet_index, ayet, ses) VALUES(?, ?, ?, ?, ?)';

                                                tx.executeSql(q, [d.id, d.sure_id, d.ayet_index, d.ayet, d.ses]);
                                            }
                                        });
                                    });
                                }, function(error) {
                                    console.log(error);
                                }, function() {
                                });
                            });
                        }
                    }, function(err) {
                        console.error(err);
                    });
                }
            }
        },

        get: function(sure_id, id, success) {
            if (!sure_id) {
                if (_.isArray($localStorage.ayetler[sure_id]) && $localStorage.ayetler[sure_id].length > 0) {
                    success(_.findWhere($localStorage.ayetler[sure_id], parseInt(id)));
                } else {
                    var q = 'SELECT * FROM ayetler WHERE id=?';

                    $cordovaSQLite.execute(db, q, [id]).then(function(res) {
                        if (res.rows.length > 0) {
                            success(res.rows.item(0));
                        } else {
                            success(null);
                        }
                    });
                }
            } else {
                this.all(sure_id, function(ayetler) {
                    success(_.findWhere(ayetler, {id: parseInt(id)}));
                });
            }
        }
    };
});