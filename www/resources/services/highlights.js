Meal.factory('Highlights', function($localStorage) {
    return {
        add: function(data) {
            var highlights = [],
                key = data.type + data.id + data.index;

            if (!_.isObject($localStorage.highlights)) {
                $localStorage.highlights = {};
                $localStorage.highlights[key] = [];
            } else {
                if (!_.isArray($localStorage.highlights[key])) {
                    $localStorage.highlights[key] = [];
                }
            }

            highlights = $localStorage.highlights[key];

            if (!_.contains(highlights, data.highlight)) {
                highlights.push(data.highlight);
            }

            $localStorage.highlights[key] = highlights.sort();
        },

        get: function(type, id, index) {
            if (_.isObject(($localStorage.highlights))) {
                return $localStorage.highlights[type + id + index] || [];
            }

            return [];
        },

        has: function(type, id) {
            if (_.isObject(($localStorage.highlights))) {
                var found =false,
                    i = 0;

                do {
                    found = _.isArray($localStorage.highlights[type + id + i]) && $localStorage.highlights[type + id + i].length > 0;

                    if (i > 25) {
                        break;
                    } else {
                        i++;
                    }

                } while (!found);

                return found;
            } else {
                return false;
            }
        },

        remove: function(type, id, index) {
            if (_.isObject(($localStorage.highlights))) {
                $localStorage.highlights[type + id + index] = [];
            }
        }
    };
});