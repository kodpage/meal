Meal.factory('Sureler', function($localStorage, Data, $cordovaSQLite, Settings) {
    if (!_.isArray($localStorage.sureler)) {
        $localStorage.sureler = [];
    }

    // Downloaded
    if (!_.isArray($localStorage.downloadedSureler)) {
        $localStorage.downloadedSureler = [];
    }

    var checkDownload = function(sureler) {
        return sureler.map(function(s) {
            s.downloaded = _.contains($localStorage.downloadedSureler, s.id);

            return s;
        });
    };

    return {
        all: function(success) {
            var sureler = [],
                that = this;

            if (Settings.get('netUsageType') != 'online') {
                if (_.isArray($localStorage.sureler) && $localStorage.sureler.length > 0) {
                    sureler = $localStorage.sureler.map(function(d) {
                        d.id = parseInt(d.id);
                        d.sirasi = parseInt(d.sirasi);
                        d.inis_sirasi = parseInt(d.inis_sirasi);
                        d.ayet_sayisi = parseInt(d.ayet_sayisi);

                        return d;
                    });

                    success(checkDownload(sureler));
                } else {
                    var q = 'SELECT * FROM sureler';

                    $cordovaSQLite.execute(db, q, []).then(function(res) {
                        if (res.rows.length > 0) {
                            for (var i = 0; i < res.rows.length; i++) {
                                sureler.push(res.rows.item(i));
                            }

                            sureler = checkDownload(sureler);
                            $localStorage.sureler = sureler;
                            success(sureler);
                        } else {
                            Data.get('sureler', function(data) {
                                sureler = data.map(function(d) {
                                    d.id = parseInt(d.id);
                                    d.sirasi = parseInt(d.sirasi);
                                    d.inis_sirasi = parseInt(d.inis_sirasi);
                                    d.ayet_sayisi = parseInt(d.ayet_sayisi);

                                    return d;
                                });

                                sureler = checkDownload(sureler);
                                $localStorage.sureler = sureler;
                                success(sureler);

                                // Store in db
                                db.transaction(function(tx) {
                                    _.each(sureler, function(d) {
                                        tx.executeSql('SELECT * FROM sureler WHERE id=?', [d.id], function(tx, res) {
                                            if (res.rows.length < 1) {
                                                q = 'INSERT INTO sureler (id, ismi, ismi_cleaned, aciklamasi, ayet_sayisi, sirasi, inis_sirasi, vidyo) VALUES(?, ?, ?, ?, ?, ?, ?, ?)';

                                                tx.executeSql(q, [d.id, d.ismi, d.ismi_cleaned, d.aciklamasi, d.ayet_sayisi, d.sirasi, d.inis_sirasi, d.vidyo]);
                                            }
                                        });
                                    });
                                }, function(error) {
                                    console.log(error);
                                }, function() {
                                });
                            });
                        }
                    }, function(err) {
                        console.error(err);
                    });
                }
            } else {
                Data.get('sureler', function(data) {
                    sureler = data.map(function(d) {
                        d.id = parseInt(d.id);
                        d.sirasi = parseInt(d.sirasi);
                        d.inis_sirasi = parseInt(d.inis_sirasi);
                        d.ayet_sayisi = parseInt(d.ayet_sayisi);

                        return d;
                    });

                    success(checkDownload(sureler));
                });
            }
        },

        get: function(id, success) {
            var that = this;

            if (_.isArray($localStorage.sureler) && $localStorage.sureler.length > 0) {
                var sure = _.find($localStorage.sureler, {id: parseInt(id)});

                success(sure);
            } else {
                var q = 'SELECT * FROM sureler WHERE id=?';

                $cordovaSQLite.execute(db, q, [id]).then(function(res) {
                    if (res.rows.length > 0) {
                        success(res.rows.item(0));
                    } else {
                        that.all(function(sureler) {
                            success(_.findWhere(sureler, {id: parseInt(id)}));
                        });
                    }
                });
            }
        }
    };
});