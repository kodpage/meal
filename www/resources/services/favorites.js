Meal.factory('Favorites', function($localStorage) {
    return {
        all: function() {
            return $localStorage.favorites || [];
        },

        get: function(id) {
            return _.findWhere(this.all(), {id: id});
        },

        add: function(data) {
            if (!_.isArray($localStorage.favorites)) {
                $localStorage.favorites = [];
            }

            $localStorage.favorites.push(data);
        },

        remove: function(id) {
            $localStorage.favorites = _.filter(this.all(), function(favorite) {
                return favorite.id != id;
            });
        },

        isFavorite: function(id) {
            return !!this.get(id);
        }
    };
});