Meal.factory('ShareGenerator', function($filter) {

    return {
        generate: function(text, subText, success) {
            var canvas = document.createElement('canvas'),
                context = canvas.getContext('2d'),
                options = {
                    maxWidth: 768,
                    minWidth: 480,
                    gapsHeight: 45,
                    padding: 45,
                    lineHeight: 27,
                    font: '18px DINPro-Medium',
                    subFont:  '12px DINPro'
                };

            var paragraphs = $filter('nl2p')(text).match(/<p[^>]*>([\s\S]*?)<\/p>/g).map(function(p) {
                return p.replace('<p>', '').replace('</p>', '').trim();
            });

            var calculateCanvasSize = function() {
                var canvasHeight = 0,
                    lineWidth = 0,
                    maxLineContentWidth = options.maxWidth - (options.padding * 2);

                context.font = options.font;

                _.each(paragraphs, function(p) {
                    var words = p.split(' '),
                        line = '';

                    for (var n = 0; n < words.length; n++) {
                        var testLine = line + words[n] + ' ',
                            metrics = context.measureText(testLine),
                            testWidth = metrics.width;

                        if (testWidth > maxLineContentWidth && n > 0) {
                            line = words[n] + ' ';

                            canvasHeight += options.lineHeight;
                        } else {
                            line = testLine;

                            if (lineWidth > 0) {
                                if (lineWidth < context.measureText(testLine).width) {
                                    lineWidth = context.measureText(testLine).width;
                                }
                            } else {
                                lineWidth = context.measureText(testLine).width;
                            }
                        }
                    }

                    canvasHeight += options.lineHeight * 1.8;
                });

                return {
                    width: lineWidth + (options.padding * 2),
                    height: canvasHeight + (options.padding * 2) + options.gapsHeight
                };
            };

            var drawBackground = function(dimensions, success) {
                // Draw flat background
                context.fillStyle = "#861919";
                context.fillRect(0, 0, dimensions.width, dimensions.height);

                // Draw sub-text background
                var layer1 = new Image();
                layer1.src = 'assets/img/layer-1.png';
                layer1.addEventListener('load', function() {
                    context.font = options.subFont;
                    context.fillStyle = '#fff';

                    var tx = (dimensions.width - (options.padding + context.measureText(subText).width)) + 15,
                        ty = dimensions.height - 51;

                    context.fillText(subText, tx, ty);

                    var l1w = context.measureText(subText).width + 60 + options.padding,
                        l1x = tx - 45;

                    context.drawImage(layer1, l1x, dimensions.height - 75, l1w, 40);

                    var layer2 = new Image();
                    layer2.src = 'assets/img/layer-2.png';
                    layer2.addEventListener('load', function() {
                        var l2x = l1x - 25;

                        context.drawImage(layer2, l2x, dimensions.height - 75, 40, 40);

                        success();
                    });
                }, false);
            };

            var writeText = function() {
                var x = options.padding,
                    y = options.padding,
                    maxLineContentWidth = options.maxWidth - (options.padding * 2);

                context.font = options.font;
                context.fillStyle = '#fff';

                _.each(paragraphs, function(p) {
                    var words = p.split(' '),
                        line = '';

                    for (var n = 0; n < words.length; n++) {
                        var testLine = line + words[n] + ' ',
                            metrics = context.measureText(testLine),
                            testWidth = metrics.width;

                        if (testWidth > maxLineContentWidth && n > 0) {
                            context.fillText(line, x, y);
                            line = words[n] + ' ';
                            y += options.lineHeight;
                        } else {
                            line = testLine;
                        }
                    }

                    context.fillText(line, x, y);
                    y = y + options.lineHeight * 1.8;
                    context.fillText(' ', x, y);
                });
            };

            // Calculate size
            var d = calculateCanvasSize();

            if (d.width <= options.minWidth) {
                d.width = options.minWidth + 60;
            }

            canvas.width = canvas.style.width = d.width;
            canvas.height = canvas.style.height = d.height;

            // Draw bg and text
            drawBackground(d, function() {
                // Write text
                writeText();

                success(canvas);
            });
        }
    };
});