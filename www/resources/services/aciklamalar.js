Meal.factory('Aciklamalar', function($localStorage, $cordovaSQLite, Data, Settings) {
    return {
        all: function(ayet_id, success) {
            var aciklamalar = [],
                that = this;

            if (Settings.get('netUsageType') == 'online') {
                Data.get('aciklamalar/' + ayet_id, function(data) {
                    aciklamalar = data.map(function(d) {
                        d.id = parseInt(d.id);
                        d.ayet_id = parseInt(d.ayet_id);
                        d.aciklama_index = parseInt(d.aciklama_index);
                        d.pozisyon =  parseInt(d.pozisyon);

                        return d;
                    });

                    success(aciklamalar);
                });
            } else {
                var q = 'SELECT * FROM aciklamalar WHERE ayet_id=?';

                $cordovaSQLite.execute(db, q, [ayet_id]).then(function(res) {
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            aciklamalar.push(res.rows.item(i));
                        }

                        success(aciklamalar);
                    } else {
                        Data.get('aciklamalar/' + ayet_id, function(data) {
                            aciklamalar = data.map(function(d) {
                                d.id = parseInt(d.id);
                                d.ayet_id = parseInt(d.ayet_id);
                                d.aciklama_index = parseInt(d.aciklama_index);
                                d.pozisyon =  parseInt(d.pozisyon);

                                return d;
                            });

                            success(aciklamalar);

                            db.transaction(function(tx) {
                                _.each(aciklamalar, function(d) {
                                    tx.executeSql('SELECT * FROM aciklamalar WHERE id=?', [d.id], function(tx, res) {
                                        if (res.rows.length < 1) {
                                            q = 'INSERT INTO aciklamalar (id, ayet_id, aciklama_index, aciklama, pozisyon) VALUES(?, ?, ?, ?, ?)';

                                            tx.executeSql(q, [d.id, d.ayet_id, d.aciklama_index, d.aciklama, d.pozisyon]);
                                        }
                                    });
                                });
                            }, function(error) {
                                console.log(error);
                            }, function() {
                            });
                        });
                    }
                }, function(err) {
                    console.error(err);
                });
            }
        },

        get: function(id, success) {
            var q = 'SELECT * FROM aciklamalar WHERE id=?';

            $cordovaSQLite.execute(db, q, [id]).then(function(res) {
                if (res.rows.length > 0) {
                    success(res.rows.item(0));
                } else {
                    success(null);
                }
            });
        }
    };
});