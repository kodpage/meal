Meal.factory('Settings', function($rootScope, $localStorage) {
    var defaults = {
        fontSize: 'normal',
        showAyetArabic: true,
        showAyetlerViewMode: 'slide',
        sureOrder: 'mushaf',
        netUsageType: 'partial-online'
    };

    // Initialize settings
    if (!_.isObject($localStorage.settings)) {
        $localStorage.settings = defaults;
    }

    $rootScope.settings = $localStorage.settings;

    return {
        all: function() {
            return $rootScope.settings;
        },

        get: function(key) {
            return $rootScope.settings[key];
        },

        set: function(key, value) {
            $localStorage.settings[key] = value;
            $rootScope.settings[key] = value;
        }
    };
});