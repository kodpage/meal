Meal.factory('Notes', function($cordovaSQLite) {

    return {
        all: function(success) {
            var notes = [];

            $cordovaSQLite.execute(db, 'SELECT * FROM notes', []).then(function(res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var note = res.rows.item(i);

                        note.text = note.note;
                        note.date = moment(note.date);

                        notes.push(note);
                    }
                }

                notes = _.sortBy(notes, function(note) {
                    return note.date.unix()
                }).reverse();

                success(notes);
            });
        },

        get: function(id, success) {
            var note = null;

            $cordovaSQLite.execute(db, 'SELECT * FROM notes WHERE id=?', [id]).then(function(res) {
                if (res.rows.length > 0) {
                    note = res.rows.item(0);

                    note.text = note.note;
                    note.date = moment(note.date);
                }

                success(notes);
            });
        },

        add: function(note, success) {
            var ayetId = note.ayet_id || null;

            if (!_.isFunction(success)) {
                success = function() {

                };
            }

            $cordovaSQLite.execute(db, 'INSERT INTO notes (note, date, ayet_id) VALUES(?, ?, ?)', [note.text, moment().format(), ayetId]).then(function() {
                success();
            });
        },

        update: function(note, success) {
            note.date = moment().format();

            if (!_.isFunction(success)) {
                success = function() {

                };
            }

            $cordovaSQLite.execute(db, 'UPDATE notes SET note=?, date=? WHERE id=?', [note.text, note.date, note.id]).then(function() {
                success();
            });
        },

        remove: function(id, success) {
            if (!_.isFunction(success)) {
                success = function() {

                };
            }

            $cordovaSQLite.execute(db, 'DELETE FROM notes WHERE id=?', [id]).then(function() {
                success();
            });
        },

        set: function(note, success) {
            if (note.id) {
                this.update(note, success);
            } else {
                this.add(note, success);
            }
        },

        ayetNotes: function(ayetId, success) {
            $cordovaSQLite.execute(db, 'SELECT * FROM notes WHERE ayet_id=?', [ayetId]).then(function(res) {
                var notes = [];

                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var note = res.rows.item(i);

                        note.text = note.note;
                        note.date = moment(note.date);

                        notes.push(note);
                    }
                }

                notes = _.sortBy(notes, function(note) {
                    return note.date.unix()
                }).reverse();

                success(notes);
            });
        }
    };
});