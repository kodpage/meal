Meal.factory('Data', function($http, urls) {
    return {
        get: function(resource, success) {
            $http.get(urls.BASE_API + '/' + resource).success(function(data) {
                success(data);
            });
        },

        post: function(resource, data, success) {
            $http.post(urls.BASE_API + '/' + resource, data).success(function(data) {
                success(data);
            });
        }
    };
});