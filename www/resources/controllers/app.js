Meal.controller('AppCtrl', function($scope, $state, $cordovaInAppBrowser) {
    $scope.goBack = function() {
        window.history.back();
    };

    $scope.openInAppBrowser = function(url) {
        var options = {
            location: 'yes',
            clearcache: 'yes',
            toolbar: 'yes'
        };

        $cordovaInAppBrowser.open(url, '_blank', options);
    };

    $scope.iosInputFix = function() {
        $scope.$emit('ios-input-fix');
    }
});