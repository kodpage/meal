Meal.controller('SettingsCtrl', function($scope, $localStorage, $ionicLoading, $ionicPopup, $cordovaSQLite, Data, Settings) {
    $scope.settings = Settings.all();

    $scope.$watch('settings', function(newVal, oldVal) {
        _.each($scope.settings, function(value, key) {
            Settings.set(key, value);
        });

        if (newVal.netUsageType != oldVal.netUsageType) {
            $cordovaSQLite.execute(db, 'DELETE FROM sureler');
            $cordovaSQLite.execute(db, 'DELETE FROM ayetler');
            $cordovaSQLite.execute(db, 'DELETE FROM aciklamalar');
            $cordovaSQLite.execute(db, "DELETE FROM SQLITE_SEQUENCE WHERE name='sureler'");
            $cordovaSQLite.execute(db, "DELETE FROM SQLITE_SEQUENCE WHERE name='ayetler'");
            $cordovaSQLite.execute(db, "DELETE FROM SQLITE_SEQUENCE WHERE name='aciklamalar'");

            if (_.isArray($localStorage.sureler) && $localStorage.sureler.length > 0) {
                $localStorage.sureler = [];
            }

            if (_.isObject($localStorage.ayetler)) {
                $localStorage.ayetler = {};
            }

            $localStorage.downloadedSureler = [];

            if (newVal.netUsageType == 'offline') {
                $ionicPopup.confirm({
                    title: 'Tüm uygulama verisi indirilecek!',
                    template: 'Bu işlem internet hızınız ve cihazınızın performansına bağlı olarak 10 saniye ile 2 dakika arasında sürecektir. İşlemi gerçekleştirmek istiyor musunuz?',
                    cancelText: 'İptal',
                    okText: 'Evet'
                }).then(function(res) {
                    if (res) {
                        var q = '';

                        $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

                        Data.get('everything', function(data) {
                            db.transaction(function(tx) {
                                _.each(data['sureler'], function(d) {
                                    d.id = parseInt(d.id);
                                    d.sirasi = parseInt(d.sirasi);
                                    d.inis_sirasi = parseInt(d.inis_sirasi);
                                    d.ayet_sayisi = parseInt(d.ayet_sayisi);

                                    $localStorage.downloadedSureler.push(d.id);

                                    q = 'INSERT INTO sureler (id, ismi, ismi_cleaned, aciklamasi, ayet_sayisi, sirasi, inis_sirasi, vidyo) VALUES(?, ?, ?, ?, ?, ?, ?, ?)';

                                    tx.executeSql(q, [d.id, d.ismi, d.ismi_cleaned, d.aciklamasi, d.ayet_sayisi, d.sirasi, d.inis_sirasi, d.vidyo]);
                                });

                                _.each(data['ayetler'], function(d) {
                                    d.id = parseInt(d.id);
                                    d.sure_id = parseInt(d.sure_id);
                                    d.ayet_index = parseInt(d.ayet_index);

                                    q = 'INSERT INTO ayetler (id, sure_id, ayet_index, ayet, ses) VALUES(?, ?, ?, ?, ?)';

                                    tx.executeSql(q, [d.id, d.sure_id, d.ayet_index, d.ayet, d.ses]);
                                });

                                _.each(data['aciklamalar'], function(d) {
                                    d.id = parseInt(d.id);
                                    d.ayet_id = parseInt(d.ayet_id);
                                    d.aciklama_index = parseInt(d.aciklama_index);

                                    q = 'INSERT INTO aciklamalar (id, ayet_id, aciklama_index, aciklama, pozisyon) VALUES(?, ?, ?, ?, ?)';

                                    tx.executeSql(q, [d.id, d.ayet_id, d.aciklama_index, d.aciklama, d.pozisyon]);
                                });
                            }, function(error) {
                                console.log(error);
                            }, function() {
                                $ionicLoading.hide();
                            });
                        });
                    } else {
                        $scope.settings.netUsageType = oldVal.netUsageType;
                        Settings.set('netUsageType', oldVal.netUsageType);
                    }
                });
            }
        }
    }, true);
});