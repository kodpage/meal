Meal.controller('QuranSearchCtrl', function($scope, $rootScope, $state, $ionicPopover, $ionicLoading, $cordovaSQLite, Data, Notes, Settings) {
    $scope.results = [];
    $scope.searchFilter = 'all';
    $scope.query = '';
    $scope.results = [];

    $ionicPopover.fromTemplateUrl('resources/views/quran/search-filters-popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.filtersPopover = popover;
    });
    $scope.isFilter = function(filter) {
        return $scope.searchFilter == filter;
    };
    $scope.setFilter = function(filter) {
        $scope.searchFilter = filter;
        $scope.filtersPopover.hide();
    };

    $scope.getAyetlerLink = function(id) {
        if (Settings.get('showAyetlerViewMode') == 'book') {
            return '#/app/quran/ayetler-book/' + id;
        } else {
            return '#/app/quran/ayetler-slide/' + id;
        }
    };

    $scope.startSearch = function() {
        if ($scope.query && $scope.query.length > 2) {
            $scope.results = [];

            var key = $scope.searchFilter + '-' + $scope.query;

            $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

            // Search in pre searched terms pool
            $cordovaSQLite.execute(db, 'SELECT * FROM search_pool WHERE term=?', [key]).then(function(res) {
                if (res.rows.length > 0) {
                    var results = JSON.parse(res.rows.item(0).result);

                    $scope.results.push.apply($scope.results, results);

                    if ($scope.searchFilter == 'all' || $scope.searchFilter == 'note') {
                        Notes.all(function(notes) {
                            _.find(notes, function(note) {
                                var regex = new RegExp($scope.query, 'ig');

                                if (note.text.search(regex) >= 0) {
                                    $scope.results.push({
                                        type: 'note',
                                        text: note.text
                                    });
                                }
                            });
                        });
                    }

                    $ionicLoading.hide();
                } else {
                    Data.post('search', {query: $scope.query, type: $scope.searchFilter}, function(data) {
                        $scope.results.push.apply($scope.results, data.results);

                        if ($scope.searchFilter == 'all' || $scope.searchFilter == 'note') {
                            Notes.all(function(notes) {
                                _.find(notes, function(note) {
                                    var regex = new RegExp($scope.query, 'ig');

                                    if (note.text.search(regex) >= 0) {
                                        $scope.results.push({
                                            type: 'note',
                                            text: note.text
                                        });
                                    }
                                });
                            });
                        }

                        $ionicLoading.hide();

                        // Save in local db
                        $cordovaSQLite.execute(db, 'INSERT INTO search_pool (term, result) VALUES (?, ?)', [key, JSON.stringify(data.results)]);
                    });
                }
            });
        }
    };

    $scope.markupText = function(text) {
        var term = $scope.query || '';

        if (term && term.length > 1) {
            var regex = new RegExp('(' + term + ')', 'ig');

            return text.replace(regex, '<span class="search-term">$1</span>');
        } else {
            return text;
        }
    };

    $rootScope.$on('$stateChangeStart', function() {
        $scope.filtersPopover.remove();
    });
});