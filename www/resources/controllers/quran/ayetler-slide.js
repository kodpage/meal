Meal.controller('QuranAyetlerSlideCtrl', function
    ($scope, $rootScope, $filter, $state, $stateParams, $timeout,
     $cordovaSQLite, $cordovaDialogs, $localStorage,
     $ionicModal, $ionicPopover, $ionicSlideBoxDelegate, $ionicPopup,
     $ionicLoading, $cordovaInstagram, $cordovaSocialSharing,
     Sureler, Ayetler, Favorites, Aciklamalar, Notes, Data, Settings, Audio, ShareGenerator) {

    $scope.sure = [];
    $scope.ayetler = [];
    $scope.orginalAyetler = [];
    $scope.ayet = {};
    $scope.notes = [];
    $scope.aciklamalar = [];
    $scope.aciklama = {};
    $scope.searched = false;
    $scope.showArabic = Settings.get('showAyetArabic');
    $scope.viewMode = 'slide';
    $scope.itemLimit = 20;

    // Sure
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    Sureler.get($stateParams.sure_id, function(sure) {
        $scope.sure = sure;

        // Get ayetler
        Ayetler.all($scope.sure.id, function(ayetler) {
            $scope.ayet = ayetler[0];
            $scope.ayetler = ayetler;
            $scope.orginalAyetler = ayetler;

            $timeout(function() {
                $ionicSlideBoxDelegate.update();
            });

            // Get ayet notes
            Notes.ayetNotes($scope.ayet.id, function(notes) {
                $scope.notes = notes;
            });

            // Get ayet aciklamalar
            Aciklamalar.all($scope.ayet.id, function(aciklamalar) {
                $scope.aciklamalar = aciklamalar;
            });

            $ionicLoading.hide();
        });
    });

    $scope.getArabic = function(ayet) {
        return quranArabic[$scope.sure.sirasi + '-' + ayet.ayet_index];
    };

    $scope.slideChanged = function(index) {
        $scope.ayet = $scope.ayetler[index];

        // Get ayet notes
        Notes.ayetNotes($scope.ayet.id, function(notes) {
            $scope.notes = notes;
        });

        // Get ayet aciklamalar
        Aciklamalar.all($scope.ayet.id, function(aciklamalar) {
            $scope.aciklamalar = aciklamalar;
        });

        if ($scope.itemLimit - (index + 1) < 10) {
            $scope.itemLimit += 10;

            $timeout(function() {
                $ionicSlideBoxDelegate.update();
            });
        }

        if (_.isObject($scope.media)) {
            $scope.media.pause();
            $scope.mediaRunning = false;
            $scope.media = null;
        }
    };

    // Ayetler menu
    $ionicPopover.fromTemplateUrl('resources/views/quran/ayetler-popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.morePopover = popover;
    });

    // Sure aciklamasi
    $ionicModal.fromTemplateUrl('resources/views/quran/sure-info-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.infoModal = modal;
    });
    $scope.showSureAciklamasi = function() {
        $timeout(function() {
            $scope.$emit('ayetLoaded');
        }, 300);

        $scope.infoModal.show();
        $scope.morePopover.hide();
    };

    // Sure video
    $scope.showSureVideo = function() {
        YoutubeVideoPlayer.openVideo($scope.sure.vidyo);
    };

    $scope.markupText = function(ayet) {
        var term = $scope.query || '';

        if (term && term.length > 1 && $scope.searched) {
            var regex = new RegExp('(' + term + ')', 'ig');

            return ayet.replace(regex, '<span class="search-term">$1</span>');
        } else {
            return ayet;
        }
    };

    // Ayet aciklamasi
    $ionicModal.fromTemplateUrl('resources/views/quran/aciklama-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.aciklamaModal = modal;
    });
    $('ion-view').off('click').on('click', '.aciklama-position', function(e) {
        e.stopPropagation();

        var ayetPosition = parseInt($(this).text().trim());

        $scope.$apply(function() {
            $scope.aciklama = _.findWhere($scope.aciklamalar, {ayet_id: $scope.ayet.id, pozisyon: ayetPosition});

            $timeout(function() {
                $scope.$emit('ayetLoaded');
            });

            $scope.aciklamaModal.show();
        });

        return false;
    });

    $scope.startSearch = function() {
        var term = $scope.query || '',
            filtered = [];

        if (term) {
            var ayetIndex = parseInt(term);

            if (!isNaN(ayetIndex)) {
                var result = _.findWhere($localStorage.ayetler[$scope.sure.id], {'ayet_index': ayetIndex});

                if (_.isObject(result)) {
                    filtered.push(result);
                }
            } else {
                $scope.searched = true;

                _.each($localStorage.ayetler[$scope.sure.id], function(ayet) {
                    if (ayet.ayet.search(new RegExp(term, 'ig')) !== -1) {
                        filtered.push(ayet);
                    }
                });
            }
        } else {
            $scope.searched = false;

            filtered = $scope.orginalAyetler;
        }

        $scope.ayetler = filtered;

        $timeout(function() {
            $ionicSlideBoxDelegate.update();
        }, 50);
    };

    $scope.$watch('query', function(newVal, oldVal) {
        if (newVal != oldVal) {
            if (!newVal) {
                $scope.searched = false;

                $scope.ayetler = $scope.orginalAyetler;

                $timeout(function() {
                    $ionicSlideBoxDelegate.update();
                }, 50);
            }
        }
    });

    $scope.morePopoverHeight = function() {
        return $scope.sure.vidyo ? 220 : 170;
    };

    $scope.changeViewMode = function() {
        $state.go('app.quran.ayetler-book', {sure_id: $stateParams.sure_id});
        $scope.morePopover.hide();
    };

    // Favorite
    $scope.isFavorite = function(id) {
        return Favorites.isFavorite(id);
    };
    $scope.toggleFavorite = function() {
        if (Favorites.isFavorite($scope.ayet.id)) {
            Favorites.remove($scope.ayet.id);
        } else {
            Favorites.add({
                id: $scope.ayet.id,
                ayet: $scope.ayet,
                sure: $scope.sure
            });
        }

        $scope.morePopover.hide();
    };

    // Share ayet
    $scope.shareTargets = [
        {type: 'gallery', text: 'Galeriye Kaydet', checked: false},
        {type: 'facebook', text: 'Facebook', checked: false},
        {type: 'twitter', text: 'Twitter', checked: false}
    ];
    if (window.cordova) {
        $cordovaInstagram.isInstalled(function(err, installed) {
            if (installed) {
                $scope.shareTargets.push({type: 'instagram', text: 'Instagram', checked: false});
            }
        });

        $cordovaSocialSharing.canShareVia('whatsapp').then(function() {
            $scope.shareTargets.push({type: 'whatsapp', text: 'Whatsapp', checked: false});
        });
    }
    $scope.updateTargets = function(index) {
        for (var i = 0; i < $scope.shareTargets.length; i++) {
            $scope.shareTargets[i].checked = i == index;
        }
    };
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-share-targets-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.shareTargetsModal = modal;
    });

    $scope.startShare = function() {
        var text = ($scope.multiShareText)
            ? $scope.multiShareText
            : $scope.ayet.ayet.replace(/\|[0-9]+\|/g, ''),
            subText = ($scope.multiShareSubText)
                ? $scope.multiShareSubText
                : 'Hayat Kitabı Kur\'an - ' + $scope.sure.ismi + ' Suresi ' + $scope.ayet.ayet_index + '. Ayet';

        ShareGenerator.generate(text, subText, function(canvas) {
            var src = canvas.toDataURL();

            _.each($scope.shareTargets, function(target) {
                if (target.checked) {
                    switch (target.type) {
                        case 'facebook':
                            $cordovaSocialSharing.shareViaFacebook(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Facebook paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'twitter':
                            $cordovaSocialSharing.shareViaTwitter(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Twitter paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'instagram':
                            $cordovaInstagram.share(src, null).then(function() {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Instagram paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'whatsapp':
                            $cordovaSocialSharing.shareViaWhatsApp(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Whatsapp paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'gallery':
                            window.canvas2ImagePlugin.saveImageDataToLibrary(
                                function(msg) {
                                    $cordovaDialogs.alert('Paylaşım başarıyla galeriye kaydedildi.', 'Başarılı!');
                                },
                                function(err) {
                                    $cordovaDialogs.alert('Galeriye kayderken hata oluştu!');
                                },
                                canvas
                            );
                            break;
                    }
                }
            });

            $scope.shareTargetsModal.hide();
            $scope.multiShareText = '';
            $scope.multiShareSubText = '';
        });
    };

    // Multi share
    $scope.multiShareAyetler = [];
    $scope.multiShareText = '';
    $scope.multiShareSubText = '';
    $ionicModal.fromTemplateUrl('resources/views/quran/ayetler-multi-share-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.multiShareModal = modal;
    });
    $scope.startMultiShare = function() {
        $scope.multiShareAyetler = [];

        $scope.orginalAyetler.map(function(ayet) {
            $scope.multiShareAyetler.push({
                text: $scope.sure.ismi + ' Suresi ' + ayet.ayet_index + '. Ayet',
                id: ayet.id,
                index: ayet.ayet_index,
                ayet: ayet,
                checked: false
            });
        });

        $scope.multiShareModal.show();
        $scope.morePopover.hide();
    };
    $scope.continueMultiShare = function() {
        var text = '',
            subText = 'Hayat Kitabı Kur\'an - ' + $scope.sure.ismi + ' Suresi';

        $scope.multiShareAyetler.map(function(item) {
            if (item.checked) {
                var ayet = item.ayet;

                text += ayet.ayet.replace(/\|[0-9]+\|/g, '') + ' ';
            }
        });
        text = text.trim();

        if (text) {
            $scope.multiShareText = text;
            $scope.multiShareSubText = subText;

            $scope.multiShareModal.hide();
            $scope.shareTargetsModal.show();
        } else {
            $cordovaDialogs.alert('İşleme devam edebilmek için en az bir ayet seçmelisiniz!');
            return false;
        }
    };

    // Audio
    $scope.mediaRunning = false;
    $scope.media = null;
    $scope.playAudio = function(e) {
        if (!$scope.mediaRunning) {
            $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

            Audio.load($scope.ayet.ses, null, function(status) {
                if (status == 1) {
                    $scope.mediaRunning = true;
                }

                if (status == 4) {
                    $scope.mediaRunning = false;
                }
            }).then(function(media) {
                $scope.mediaRunning = true;
                media.play();
                $ionicLoading.hide();

                $scope.media = media;
            });
        }
    };
    $scope.pauseAudio = function() {
        if (_.isObject($scope.media)) {
            $scope.media.pause();
            $scope.mediaRunning = false;
            $scope.media = null;
        }
    };

    // Ayet Notes
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-notes-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.ayetNotesModal = modal;
    });
    $ionicModal.fromTemplateUrl('resources/views/quran/notes-note-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.noteModal = modal;
    });
    $scope.showNote = function(note) {
        if (note) {
            $scope.note = note;
        } else {
            $scope.note = {text: ''};
        }

        $scope.ago = function() {
            var now = moment();

            return now.to($scope.note.date);
        };

        $scope.noteModal.show();
    };
    $scope.saveNote = function() {
        if ($scope.note.text && $scope.note.text.length >= 3) {
            $scope.note.ayet_id = $scope.ayet.id;

            Notes.set($scope.note);

            if ($scope.note.id) {
                $scope.notes = $scope.notes.filter(function(note) {
                    return note.id != $scope.note.id;
                });
            }

            $scope.note.date = moment().format();

            $scope.notes.unshift($scope.note);

            $scope.noteModal.hide();
        } else {
            $cordovaDialogs.alert('Lütfen en az 3 karekter girdikten sonra kayıt işlemine devam ediniz.', 'Geçersiz not metni!');
        }
    };
    $scope.delete = function() {
        $ionicPopup.confirm({
            title: 'Notu sil!',
            template: 'Notu silmek istediğinize emin misiniz?',
            cancelText: 'İptal',
            okText: 'Evet'
        }).then(function(res) {
            if(res) {
                Notes.remove($scope.note.id);

                $scope.notes = $scope.notes.filter(function(note) {
                    return note.id != $scope.note.id;
                });

                $scope.noteModal.hide();
            }
        });
    };
    $scope.$on('modal.shown', function(event) {
        $('textarea', event.targetScope.noteModal.$el).trigger('change');
    });

    $rootScope.$on('$stateChangeStart', function() {
        if (_.isObject($scope.media)) {
            $scope.media.pause();
            $scope.mediaRunning = false;
            $scope.media = null;
        }

        $scope.infoModal.remove();
        $scope.shareTargetsModal.remove();
        $scope.multiShareModal.remove();
        $scope.ayetNotesModal.remove();
        $scope.noteModal.remove();

        $scope.morePopover.remove();
    });
});