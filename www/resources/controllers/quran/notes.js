Meal.controller('QuranNotesCtrl', function($scope, $rootScope, $state, $ionicModal, $ionicPopover, $ionicPopup, $cordovaDialogs, Notes) {
    $scope.notes = [];
    $scope.note = {text: ''};
    $scope.filter = 'all';

    Notes.all(function(notes) {
        $scope.notes = notes;
    });

    $ionicPopover.fromTemplateUrl('resources/views/quran/notes-filters-popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.filtersPopover = popover;
    });
    $scope.isFilter = function(filter) {
        return $scope.filter == filter;
    };
    $scope.setFilter = function(filter) {
        $scope.filter = filter;
        $scope.filtersPopover.hide();
    };
    $scope.$watch('filter', function(newVal, oldVal) {
        if (newVal != oldVal) {
            switch (newVal) {
                case 'all':
                    Notes.all(function(notes) {
                        $scope.notes = notes;
                    });
                    break;

                case 'ayet':
                    Notes.all(function(notes) {
                        $scope.notes = notes.filter(function(note) {
                            return note.ayet_id != null;
                        });
                    });
                    break;

                case 'special':
                    Notes.all(function(notes) {
                        $scope.notes = notes.filter(function(note) {
                            return note.ayet_id == null;
                        });
                    });
                    break;
            }
        }
    });

    $scope.shorten = function(text) {
        if (text.length >= 120) {
            text = text.slice(0, 120) + '...';
        }

        return text;
    };

    $scope.ago = function() {
        var now = moment();

        return now.to($scope.note.date);
    };

    $ionicModal.fromTemplateUrl('resources/views/quran/notes-note-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.noteModal = modal;
    });
    $scope.showNote = function(note) {
        if (note) {
            $scope.note = note;
        } else {
            $scope.note = {text: ''};
        }

        $scope.noteModal.show();
    };
    $scope.saveNote = function() {
        if ($scope.note.text && $scope.note.text.length >= 3) {
            Notes.set($scope.note);

            if ($scope.note.id) {
                $scope.notes = $scope.notes.filter(function(note) {
                    return note.id != $scope.note.id;
                });
            }

            $scope.note.date = moment().format();

            $scope.notes.unshift($scope.note);

            $scope.noteModal.hide();
        } else {
            $cordovaDialogs.alert('Lütfen en az 3 karekter girdikten sonra kayıt işlemine devam ediniz.', 'Geçersiz not metni!');
        }
    };
    $scope.delete = function() {
        $ionicPopup.confirm({
            title: 'Notu sil!',
            template: 'Notu silmek istediğinize emin misiniz?',
            cancelText: 'İptal',
            okText: 'Evet'
        }).then(function(res) {
            if(res) {
                Notes.remove($scope.note.id);

                $scope.notes = $scope.notes.filter(function(note) {
                    return note.id != $scope.note.id;
                });

                $scope.noteModal.hide();
            }
        });
    };

    $scope.$on('modal.shown', function(event) {
        $('textarea', event.targetScope.noteModal.$el).trigger('change');
    });

    $rootScope.$on('$stateChangeStart', function() {
        $scope.noteModal.remove();

        $scope.filtersPopover.remove();
    });
});