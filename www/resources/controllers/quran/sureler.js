Meal.controller('QuranSurelerCtrl', function($scope, $rootScope, $timeout, $localStorage, $ionicPopover, $ionicLoading, $cordovaSQLite, Sureler, Data, Settings, Ayetler, Aciklamalar) {
    $scope.downloaded = {
        ayetler: false,
        aciklamalar: false
    };

    // Get sureler
    $scope.sureler = [];
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    Sureler.all(function(sureler) {
        $scope.sureler = sureler;

        $ionicLoading.hide();
    });

    $scope.getAyetlerLink = function(id) {
        if (Settings.get('showAyetlerViewMode') == 'book') {
            return '#/app/quran/ayetler-book/' + id;
        } else {
            return '#/app/quran/ayetler-slide/' + id;
        }
    };

    $scope.download = function(sure, index, event) {
        event.preventDefault();
        event.stopPropagation();

        $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

        Data.get('everything/' + sure.id, function(data) {
            $scope.sureler[index].downloaded = true;
            $localStorage.downloadedSureler.push(sure.id);

            if (!_.isArray($localStorage.ayetler[sure.id])) {
                $localStorage.ayetler[sure.id] = [];
            }

            db.transaction(function(tx) {
                if (_.isArray(data['ayetler'])) {
                    _.each(data['ayetler'], function(ayet) {
                        var d = {
                            id: parseInt(ayet.id),
                            sure_id: parseInt(ayet.sure_id),
                            ayet_index: parseInt(ayet.ayet_index),
                            ayet: ayet.ayet,
                            ses: ayet.ses,
                        };

                        // Check if ayet in local storage.
                        if (!_.isObject(_.findWhere($localStorage.ayetler[d.sure_id], d.id))) {
                            $localStorage.ayetler[sure.id].push(d);
                        }

                        tx.executeSql('SELECT * FROM ayetler WHERE id=?', [d.id], function(tx, res) {
                            if (res.rows.length < 1) {
                                var q = 'INSERT INTO ayetler (id, sure_id, ayet_index, ayet, ses) VALUES(?, ?, ?, ?, ?)';

                                tx.executeSql(q, [d.id, d.sure_id, d.ayet_index, d.ayet, d.ses]);
                            }
                        });
                    });
                }

                if (_.isArray(data['aciklamalar'])) {
                    _.each(data['aciklamalar'], function(aciklama) {
                        var d = {
                            id: parseInt(aciklama.id),
                            ayet_id: parseInt(aciklama.ayet_id),
                            aciklama_index: parseInt(aciklama.aciklama_index),
                            aciklama: aciklama.aciklama,
                            pozisyon: aciklama.pozisyon,
                        };

                        tx.executeSql('SELECT * FROM aciklamalar WHERE id=?', [d.id], function(tx, res) {
                            if (res.rows.length < 1) {
                                var q = 'INSERT INTO aciklamalar (id, ayet_id, aciklama_index, aciklama, pozisyon) VALUES(?, ?, ?, ?, ?)';

                                tx.executeSql(q, [d.id, d.ayet_id, d.aciklama_index, d.aciklama, d.pozisyon]);
                            }
                        });
                    });
                }
            }, function(error) {
                console.log(error);
            }, function() {
                $ionicLoading.hide();
            });
        });
    };

    $scope.$watch('query', function(newVal, oldVal) {
        if (newVal != oldVal) {
            if (newVal) {
                var regex = new RegExp(newVal, 'ig'),
                    filtered = [];

                // Search sureler by ismi_cleaned
                _.each($localStorage.sureler, function(sure) {
                    if (sure.ismi_cleaned.search(regex) !== -1) {
                        filtered.push(sure);
                    }
                });

                // Search sureler by ismi
                if (filtered.length < 1) {
                    _.each($localStorage.sureler, function(sure) {
                        if (sure.ismi.search(regex) !== -1) {
                            filtered.push(sure);
                        }
                    });
                }

                $scope.sureler = filtered;
                orderSureler($scope.surelerOrder);
            } else {
                $scope.sureler = $localStorage.sureler;
                orderSureler($scope.surelerOrder);
            }
        }
    });

    // Order sureler
    var orderSureler = function(type) {
        var order = Settings.get('sureOrder');

        switch (type) {
            case 'mushaf':
                order = 'sirasi';
                break;
            case 'nuzul':
                order = 'inis_sirasi';
                break;
            case 'alfabetik':
                order = 'ismi';
                break;
        }

        if (order == 'ismi') {
            $scope.sureler = _.sortBy($scope.sureler, function(sure) {
                var text = sure.ismi;

                text = text.replace('İ', 'I');
                text = text.replace('Ş', 'S');
                text = text.replace('Â', 'A');

                return text;
            });
        } else {
            $scope.sureler = _.sortBy($scope.sureler, order);
        }
    };
    $scope.surelerOrder = Settings.get('sureOrder');
    orderSureler($scope.surelerOrder);
    $ionicPopover.fromTemplateUrl('resources/views/quran/sureler-popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.morePopover = popover;
    });
    $scope.setSurelerOrder = function(type) {
        $scope.surelerOrder = type;
        orderSureler(type);
        $scope.morePopover.hide();
    };
    $scope.showCheck = function(type) {
        var current = $scope.surelerOrder;

        if (current) {
            return current == type;
        } else {
            return type == 'mushaf';
        }
    };

    $rootScope.$on('$stateChangeStart', function() {
        $scope.morePopover.remove();
    });
});