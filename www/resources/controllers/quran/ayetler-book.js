Meal.controller('QuranAyetlerBookCtrl', function
    ($scope, $state, $stateParams, $ionicLoading, Sureler, Ayetler, $timeout, Aciklamalar, $ionicPopover, $ionicModal,
     $localStorage, $cordovaDialogs, $cordovaInstagram, ShareGenerator, $cordovaSocialSharing, $rootScope
    ) {
    $scope.sure = [];
    $scope.ayetler = [];
    $scope.orginalAyetler = [];
    $scope.aciklamalar = [];
    $scope.pageIndexes = {};
    $scope.viewMode = 'book';

    // Sure
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    Sureler.get($stateParams.sure_id, function(sure) {
        $scope.sure = sure;

        // Get ayetler
        Ayetler.all($scope.sure.id, function(ayetler) {
            $scope.ayetler = ayetler;
            $scope.orginalAyetler = ayetler;

            $timeout(function() {
                $scope.$broadcast('ayetlerLoaded');
            });
        });
    });

    // Ayetler menu
    $ionicPopover.fromTemplateUrl('resources/views/quran/ayetler-popover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.morePopover = popover;
    });

    // Sure aciklamasi
    $ionicModal.fromTemplateUrl('resources/views/quran/sure-info-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.infoModal = modal;
    });
    $scope.showSureAciklamasi = function() {
        $timeout(function() {
            $scope.$emit('ayetLoaded');
        }, 300);

        $scope.infoModal.show();
        $scope.morePopover.hide();
    };

    // Sure video
    $scope.showSureVideo = function() {
        YoutubeVideoPlayer.openVideo($scope.sure.vidyo);
    };

    $scope.changeViewMode = function() {
        $state.go('app.quran.ayetler-slide', {sure_id: $stateParams.sure_id});
        $scope.morePopover.hide();
    };
    $scope.morePopoverHeight = function() {
        return $scope.sure.vidyo ? 220 : 170;
    };

    // Search
    $scope.startSearch = function() {
        var term = $scope.query || '',
            filtered = [];

        if (term) {
            var ayetIndex = parseInt(term);

            if (!isNaN(ayetIndex)) {
                var result = _.findWhere($localStorage.ayetler[$scope.sure.id], {'ayet_index': ayetIndex});

                if (_.isObject(result)) {
                    filtered.push(result);
                }
            } else {
                $scope.searched = true;

                _.each($localStorage.ayetler[$scope.sure.id], function(ayet) {
                    if (ayet.ayet.search(new RegExp(term, 'ig')) !== -1) {
                        filtered.push(ayet);
                    }
                });
            }
        } else {
            filtered = $scope.orginalAyetler;
        }

        $scope.ayetler = filtered;

        $scope.$broadcast('ayetlerLoaded', term);
    };

    $scope.$watch('query', function(newVal, oldVal) {
        if (newVal != oldVal) {
            if (!newVal) {
                $scope.ayetler = $scope.orginalAyetler;

                $scope.$broadcast('ayetlerLoaded', 'none');
            }
        }
    });

    // Share ayet
    $scope.shareTargets = [
        {type: 'gallery', text: 'Galeriye Kaydet', checked: false},
        {type: 'facebook', text: 'Facebook', checked: false},
        {type: 'twitter', text: 'Twitter', checked: false}
    ];
    if (window.cordova) {
        $cordovaInstagram.isInstalled(function(err, installed) {
            if (installed) {
                $scope.shareTargets.push({type: 'instagram', text: 'Instagram', checked: false});
            }
        });

        $cordovaSocialSharing.canShareVia('whatsapp').then(function() {
            $scope.shareTargets.push({type: 'whatsapp', text: 'Whatsapp', checked: false});
        });
    }
    $scope.updateTargets = function(index) {
        for (var i = 0; i < $scope.shareTargets.length; i++) {
            $scope.shareTargets[i].checked = i == index;
        }
    };
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-share-targets-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.shareTargetsModal = modal;
    });

    $scope.startShare = function() {
        var text = $scope.multiShareText,
            subText = $scope.multiShareSubText;

        ShareGenerator.generate(text, subText, function(canvas) {
            var src = canvas.toDataURL();

            _.each($scope.shareTargets, function(target) {
                if (target.checked) {
                    switch (target.type) {
                        case 'facebook':
                            $cordovaSocialSharing.shareViaFacebook(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Facebook paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'twitter':
                            $cordovaSocialSharing.shareViaTwitter(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Twitter paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'instagram':
                            $cordovaInstagram.share(src, null).then(function() {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Instagram paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'whatsapp':
                            $cordovaSocialSharing.shareViaWhatsApp(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Whatsapp paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'gallery':
                            window.canvas2ImagePlugin.saveImageDataToLibrary(
                                function(msg) {
                                    $cordovaDialogs.alert('Paylaşım başarıyla galeriye kaydedildi.', 'Başarılı!');
                                },
                                function(err) {
                                    $cordovaDialogs.alert('Galeriye kayderken hata oluştu!');
                                },
                                canvas
                            );
                            break;
                    }
                }
            });

            $scope.shareTargetsModal.hide();
            $scope.multiShareText = '';
            $scope.multiShareSubText = '';
        });
    };

    // Multi share
    $scope.multiShareAyetler = [];
    $scope.multiShareText = '';
    $scope.multiShareSubText = '';
    $ionicModal.fromTemplateUrl('resources/views/quran/ayetler-multi-share-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.multiShareModal = modal;
    });
    $scope.startMultiShare = function() {
        $scope.multiShareAyetler = [];

        $scope.orginalAyetler.map(function(ayet) {
            $scope.multiShareAyetler.push({
                text: $scope.sure.ismi + ' Suresi ' + ayet.ayet_index + '. Ayet',
                id: ayet.id,
                index: ayet.ayet_index,
                ayet: ayet,
                checked: false
            });
        });

        $scope.multiShareModal.show();
        $scope.morePopover.hide();
    };
    $scope.continueMultiShare = function() {
        var text = '',
            subText = 'Hayat Kitabı Kur\'an - ' + $scope.sure.ismi + ' Suresi';

        $scope.multiShareAyetler.map(function(item) {
            if (item.checked) {
                var ayet = item.ayet;

                text += ayet.ayet.replace(/\|[0-9]+\|/g, '') + ' ';
            }
        });
        text = text.trim();

        if (text) {
            $scope.multiShareText = text;
            $scope.multiShareSubText = subText;

            $scope.multiShareModal.hide();
            $scope.shareTargetsModal.show();
        } else {
            $cordovaDialogs.alert('İşleme devam edebilmek için en az bir ayet seçmelisiniz!');
            return false;
        }
    };

    $rootScope.$on('$stateChangeStart', function() {
        $scope.infoModal.remove();
        $scope.shareTargetsModal.remove();
        $scope.multiShareModal.remove();

        $scope.morePopover.remove();
    });
});