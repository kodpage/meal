Meal.controller('QuranAyetCtrl', function(
    $scope, $filter, $timeout, $stateParams, $localStorage, $cordovaDialogs,
    $ionicModal, $ionicPopover, $ionicLoading, $rootScope,
    $cordovaSocialSharing, $cordovaInstagram, Sureler, Ayetler,
    Aciklamalar, Notes, Favorites, Settings, Audio, ShareGenerator) {
    $scope.showArabic = Settings.get('showAyetArabic');

    $scope.sure = {};
    $scope.ayet = {};
    $scope.aciklamalar = [];

    $scope.viewLoaded = false;
    $scope.viewTitle = '';

    // Sure
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    Sureler.get($stateParams.sure_id, function(sure) {
        $scope.sure = sure;

        // Ayet
        Ayetler.get($scope.sure.id, $stateParams.ayet_id, function(ayet) {
            $scope.ayet = ayet;

            //$scope.ayet.ayet = $scope.ayet.ayet.replace(/\|([0-9]+)\|/g, '<span class=""></span>');

            $scope.viewTitle = $scope.sure.ismi + ' Suresi ' + $scope.ayet.ayet_index + '. Ayet';

            // Get arabic
            if ($scope.showArabic) {
                $scope.ayet.arabic = quranArabic[$scope.sure.sirasi + '-' +  $scope.ayet.ayet_index];
            }

            Aciklamalar.all(ayet.id, function(aciklamalar) {
                $scope.aciklamalar = _.sortBy(aciklamalar, 'pozisyon');

                Notes.all(function(notes) {
                    $scope.notes = _.where(notes.reverse(), {ayet_id: ayet.id}) || [];

                    $timeout(function() {
                        $scope.$emit('ayetLoaded');

                        $scope.viewLoaded = true;

                        $ionicLoading.hide();
                    });
                });
            });
        });
    });

    // Actions
    $ionicPopover.fromTemplateUrl('resources/views/quran/ayet-popver.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.morePopover = popover;
    });

    // New note
    $scope.addNote = function() {
        $scope.note = {text: ''};
        $scope.morePopover.hide();
        $scope.noteModal.show();
    };
    $scope.saveNote = function() {
        if ($scope.note.text && $scope.note.text.length >= 3) {
            $scope.note.ayet_id = $scope.ayet.id;
            Notes.add($scope.note);
            $scope.notes.push($scope.note);
            $scope.noteModal.hide();
        } else {
            $cordovaDialogs.alert('Lütfen en az 3 karekter girdikten sonra kayıt işlemine devam ediniz.');
        }
    };
    $ionicModal.fromTemplateUrl('resources/views/quran/notes-note-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.noteModal = modal;
    });
    $scope.$on('modal.shown', function(event) {
        $('textarea', event.targetScope.noteModal.$el).trigger('change');
    });

    // Share
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-share-options-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.shareOtionsModal = modal;
    });
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-share-text-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.shareTextModal = modal;
    });
    $ionicModal.fromTemplateUrl('resources/views/quran/ayet-share-targets-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.shareTargetsModal = modal;
    });
    $scope.share = function() {
        $scope.shareOptions = [];

        // Ayet
        $scope.shareOptions.push({
            type: 'ayet',
            id: $scope.ayet.id,
            text: 'Ayet',
            checked: false
        });

        // Aciklamalar
        _.each($scope.aciklamalar, function(aciklama, index) {
            $scope.shareOptions.push({
                type: 'aciklama',
                id: aciklama.id,
                text: 'Açıklama ' + (index + 1),
                checked: false
            });
        });

        // Highlights
        $scope.highlights = [];
        $('.item-body').each(function() {
            var $container = $(this),
                text = '';

            $('p[underline] > p', $container).find('span[class*="char"]').each(function() {
                var $this = $(this);

                if ($this.hasClass('highlighted')) {
                    text += $this.text();
                } else {
                    if (text) {
                        $scope.highlights.push(text);
                        text = '';
                    }
                }
            });
        });
        _.each($scope.highlights, function(h, i) {
            $scope.shareOptions.push({
                type: 'highlight',
                id: i,
                text: 'Vurgu ' + (i + 1),
                checked: false
            });
        });

        $scope.shareOtionsModal.show();
        $scope.morePopover.hide();
    };
    $scope.saveShareOptions = function() {
        $scope.sharing = {};
        $scope.sharing.text = '';

        _.each($scope.shareOptions, function(o) {
            if (o.checked) {
                switch (o.type) {
                    case 'ayet':
                        $scope.sharing.text += $scope.ayet.ayet.replace(/\|[0-9]+\|/g, '') + '\n\n';
                        break;

                    case 'aciklama':
                        $scope.sharing.text += _.findWhere($scope.aciklamalar, {id: parseInt(o.id)}).aciklama + '\n\n';
                        break;

                    case 'highlight':
                        $scope.sharing.text += $scope.highlights[o.id] + '\n\n';
                        break;
                }
            }
        });

        if (!$scope.sharing.text) {
            $cordovaDialogs.alert('Paylaşım için hiç bir öğe seçilmedi.');
        } else {
            $scope.sharing.text = $scope.sharing.text.trim();

            $scope.shareOtionsModal.hide();
            $scope.shareTextModal.show();
        }
    };
    $scope.saveShareText = function() {
        $scope.shareTargets = [
            {type: 'gallery', text: 'Galeriye Kaydet', checked: false},
            {type: 'facebook', text: 'Facebook', checked: false},
            {type: 'twitter', text: 'Twitter', checked: false}
        ];
        if (window.cordova) {
            $cordovaInstagram.isInstalled(function(err, installed) {
                if (installed) {
                    $scope.shareTargets.push({type: 'instagram', text: 'Instagram', checked: false});
                }
            });

            $cordovaSocialSharing.canShareVia('whatsapp').then(function() {
                $scope.shareTargets.push({type: 'whatsapp', text: 'Whatsapp', checked: false});
            });
        }

        $scope.shareTextModal.hide();
        $scope.shareTargetsModal.show();
    };
    $scope.updateTargets = function(index) {
        for (var i = 0; i < $scope.shareTargets.length; i++) {
            $scope.shareTargets[i].checked = i == index;
        }
    };
    $scope.startShare = function() {
        var text = $scope.sharing.text,
            subText = 'Hayat Kitabı Kur\'an - ' + $scope.sure.ismi + ' Suresi ' + $scope.ayet.ayet_index + '. Ayet';

        ShareGenerator.generate(text, subText, function(canvas) {
            var src = canvas.toDataURL();

            _.each($scope.shareTargets, function(target) {
                if (target.checked) {
                    switch (target.type) {
                        case 'facebook':
                            $cordovaSocialSharing.shareViaFacebook(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Facebook paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'twitter':
                            $cordovaSocialSharing.shareViaTwitter(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Twitter paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'instagram':
                            $cordovaInstagram.share(src, null).then(function() {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Instagram paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'whatsapp':
                            $cordovaSocialSharing.shareViaWhatsApp(null, src, null).then(function(result) {
                                $cordovaDialogs.alert('Paylaşım başarıyla gerçekleştirildi.', 'Başarılı!');
                            }, function(err) {
                                $cordovaDialogs.alert('Whatsapp paylaşımı sırasında hata oluştu!');
                            });
                            break;

                        case 'gallery':
                            window.canvas2ImagePlugin.saveImageDataToLibrary(
                                function(msg){
                                    $cordovaDialogs.alert('Paylaşım başarıyla galeriye kaydedildi.', 'Başarılı!');
                                },
                                function(err){
                                    $cordovaDialogs.alert('Galeriye kayderken hata oluştu!');
                                },
                                canvas
                            );
                            break;
                    }
                }
            });

            $scope.shareTargetsModal.hide();
        });
    };
    $scope.$on('modal.shown', function(event) {
        $('textarea', event.targetScope.shareTextModal.$el).trigger('change');
    });

    // Favorite
    $scope.isFavorite = function(id) {
        return Favorites.isFavorite(id);
    };
    $scope.toggleFavorite = function() {
        if (Favorites.isFavorite($scope.ayet.id)) {
            Favorites.remove($scope.ayet.id);
        } else {
            Favorites.add({
                id: $scope.ayet.id,
                ayet: $scope.ayet,
                sure: $scope.sure
            });
        }

        $scope.morePopover.hide();
    };

    // Audio
    $scope.mediaRunning = false;
    $scope.media = null;
    $scope.playAudio = function(e) {
        if (!$scope.mediaRunning) {
            $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});

            Audio.load($scope.ayet.ses, null, function(status) {
                if (status == 1) {
                    $scope.mediaRunning = true;
                }

                if (status == 4) {
                    $scope.mediaRunning = false;
                }
            }).then(function(media) {
                $scope.mediaRunning = true;
                media.play();
                $ionicLoading.hide();
                $scope.morePopover.hide();

                $scope.media = media;
            });
        }
    };
    $scope.pauseAudio = function() {
        if (_.isObject($scope.media)) {
            $scope.media.pause();
            $scope.mediaRunning = false;
            $scope.media = null;

            $scope.morePopover.hide();
        }
    };
    $rootScope.$on('$stateChangeStart', function() {
        if (_.isObject($scope.media)) {
            $scope.media.pause();
            $scope.mediaRunning = false;
            $scope.media = null;
        }

        $scope.shareTargetsModal.remove();
        $scope.shareTextModal.remove();
        $scope.shareOtionsModal.remove();
        $scope.noteModal.remove();
        $scope.morePopover.remove();
    });
});