Meal.directive('underline', function($rootScope, $ionicScrollDelegate, $ionicGesture, Highlights) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            $rootScope.$on('ayetLoaded', function() {
                var $elm = $(element),
                    $container = $elm.closest('.item-body');

                // Remove highlights
                if ($('button.remove-highlights', $container).length < 1) {
                    var $removeBtn = $('<button class="button button-small button-clear button-dark icon-left ion-backspace-outline remove-highlights">Vurguları Kaldır</button>');

                    $removeBtn.on('click', function() {
                        $(this).parent().find('p[aria-label]').each(function(index) {
                            Highlights.remove(attrs.type, attrs.id, index);

                            $(this).find('span.highlighted').each(function() {
                                $(this).removeClass('highlighted');
                            });
                        });

                        $removeBtn.hide();
                    });

                    $container.prepend($removeBtn);

                    if (Highlights.has(attrs.type, attrs.id)) {
                        $removeBtn.show();
                    }
                }

                $elm.find('> p').each(function(paragraphIndex) {
                    var $this = $(this),
                        $startElm = null,
                        $endElm = null,
                        $virtualMarker = null;

                    $this.lettering();

                    // Highlights
                    var highlights = Highlights.get(attrs.type, attrs.id, paragraphIndex);

                    _.each(highlights, function(h) {
                        var calculatedClass = 'char' + h,
                            $targetElm = $this.find('span.' + calculatedClass);

                        if (!$targetElm.hasClass('highlighted')) {
                            $targetElm.addClass('highlighted');
                        }
                    });

                    $ionicGesture.on('dragstart', function(e) {
                        if (e.gesture.direction == 'right' || e.gesture.direction == 'left') {
                            var coordinates = {
                                x: e.gesture.touches[0].clientX,
                                y: e.gesture.touches[0].clientY
                            };

                            var $p = $(e.target).closest('p');

                            $startElm = $.nearest(coordinates, $p.find('> span:not(.virtual-highlighted)'), {
                                tolerance: 7,
                                container: $p
                            });

                            $virtualMarker = $('<span/>', {
                                'class': 'virtual-highlighted',
                                'css': {
                                    top: $startElm.position().top + 'px',
                                    left: $startElm.position().left + 'px',
                                    height: $startElm.height() + 'px'
                                }
                            });

                            $p.prepend($virtualMarker);
                        }
                    }, angular.element($this.get(0)));

                    $ionicGesture.on('dragright dragleft', function(e) {
                        var coordinates = {
                            x: e.gesture.touches[0].clientX,
                            y: e.gesture.touches[0].clientY
                        };

                        var c = coordinates.x - $startElm.position().left;

                        $virtualMarker.css({
                            'width': Math.abs(c) + 'px',
                            'margin-left': (c < 0) ? c + 'px' : 0
                        });
                    }, angular.element($this.get(0)));

                    $ionicGesture.on('dragend', function(e) {
                        if ($virtualMarker && $virtualMarker.length > 0) {
                            var coordinates = {
                                x: e.gesture.touches[0].clientX,
                                y: e.gesture.touches[0].clientY
                            };

                            var $p = $(e.target).closest('p');

                            $endElm = $.nearest(coordinates, $p.find('> span:not(.virtual-highlighted)'), {
                                tolerance: 1,
                                container: $p
                            });

                            // Get start class
                            var startClass = '';
                            if ($startElm.attr('class').indexOf(' ') !== -1) {
                                startClass = $startElm.attr('class').split(' ')[0];
                            } else {
                                startClass = $startElm.attr('class');
                            }

                            // Get end class
                            var endClass = '';
                            if ($endElm.attr('class').indexOf(' ') !== -1) {
                                endClass = $endElm.attr('class').split(' ')[0];
                            } else {
                                endClass = $endElm.attr('class');
                            }

                            // Get indexes;
                            var startIndex = parseInt(startClass.replace('char', '')),
                                endIndex = parseInt(endClass.replace('char', ''));

                            // Highlight
                            if (startIndex < endIndex && $virtualMarker.width() > 0) {
                                for (var i = startIndex; i <= endIndex; i++) {
                                    var calculatedClass = 'char' + i,
                                        $targetElm = $p.find('span.' + calculatedClass);

                                    Highlights.add({
                                        type: attrs.type,
                                        id: attrs.id,
                                        highlight: i,
                                        index: paragraphIndex
                                    });

                                    if (!$targetElm.hasClass('highlighted')) {
                                        $targetElm.addClass('highlighted');
                                    }
                                }

                                $removeBtn.show();
                            } else if (startIndex > endIndex && $virtualMarker.width() > 0) {
                                var temp = startIndex;
                                startIndex = endIndex;
                                endIndex = temp;

                                for (var i = startIndex; i <= endIndex; i++) {
                                    var calculatedClass = 'char' + i,
                                        $targetElm = $p.find('span.' + calculatedClass);

                                    Highlights.add({
                                        type: attrs.type,
                                        id: attrs.id,
                                        highlight: i,
                                        index: paragraphIndex
                                    });

                                    if (!$targetElm.hasClass('highlighted')) {
                                        $targetElm.addClass('highlighted');
                                    }
                                }

                                $removeBtn.show();
                            }

                            // Remove virtual marker
                            $virtualMarker.remove();
                        }
                    }, angular.element($this.get(0)));
                });
            });;
        }
    };
});