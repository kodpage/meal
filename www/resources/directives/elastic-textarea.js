Meal.directive('elastic', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, element) {
            var $el = $(element);
            $scope.initialHeight = $scope.initialHeight || $el.height();

            var resize = function() {
                element[0].style.height = $scope.initialHeight;
                element[0].style.height = "" + element[0].scrollHeight + "px";
            };

            $el.on("input change", resize);
        }
    };
}]);
