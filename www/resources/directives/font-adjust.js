Meal.directive('fontAdjust', function(Settings) {
    return {
        restrict: 'A',
        link: function($scope, element) {
            var fontSize = Settings.get('fontSize');

            $(element).addClass('font-adjusted').addClass(fontSize);
        }
    };
});