Meal.directive('iosSurelerInputFix', function($rootScope, $timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var $elm = $(element);

            $elm.attr('disabled', 'disabled');

            $timeout(function() {
                $elm.removeAttr('disabled');
            }, 400);

            $rootScope.$on('ios-input-fix', function() {
                $elm.attr('disabled', 'disabled');

                $timeout(function() {
                    $elm.removeAttr('disabled');
                }, 400);
            });
        }
    };
});