Meal.directive('ayetlerBook', function($rootScope, $compile, $timeout, $ionicLoading, $ionicModal, Ayetler, Aciklamalar) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.$on('ayetlerLoaded', function(e, searchQuery) {
                var $elm = $(element);

                // Destroy turn.js if exist.
                if ($elm.turn("is")) {
                    $elm.turn('destroy');
                }

                // Initial page items
                var h = '';
                h += '<div>';
                h += '<div class="page-item" style="opacity: 0">';
                h += '<div class="text-container"></div>';
                h += '</div>';
                h += '</div>';
                $elm.html(h);

                $timeout(function() {
                    scope.currentPage = 1;
                    scope.pageIndexes = {};
                });

                scope.markupText = function(ayet) {
                    var term = searchQuery || '';

                    if (term == 'none') {
                        term = '';
                    }

                    if (term && term.length > 1) {
                        var regex = new RegExp('(' + term + ')', 'ig');

                        return ayet.replace(regex, '<span class="search-term">$1</span>');
                    } else {
                        return ayet;
                    }
                };

                $timeout(function() {
                    var $container = $elm.closest('.scroll-content'),
                        $textContainer = $elm.find('.text-container');

                    // Adjust element height.
                    $elm.css('height', $container.height() + 'px');

                    // Init turn.js
                    $elm.turn({
                        display: 'single',
                        acceleration: true,
                        gradients: true,
                        elevation: 50,
                        turnCorners: 'bl,br,tr,tl',
                        cornerSize: 300,
                        duration: 600,
                        when: {
                            turned: function(event, page, pageObject) {
                                $timeout(function() {
                                    scope.currentPage = page;

                                    var ayetIds = scope.pageIndexes[page];

                                    if (_.isArray(ayetIds) && ayetIds.length > 0) {
                                        _.each(ayetIds, function(id, index) {
                                            // Get ayet aciklamalar
                                            Aciklamalar.all(id, function(aciklamalar) {
                                                scope.aciklamalar = scope.aciklamalar.concat(aciklamalar);

                                                if (index == ayetIds.length - 1) {
                                                    $ionicLoading.hide();
                                                }
                                            });
                                        });
                                    } else  {
                                        $ionicLoading.hide();
                                    }
                                });
                            },
                            turning: function(event, page, pageObject) {
                                $timeout(function() {
                                    scope.currentPage = page;
                                });
                            }
                        }
                    });

                    // Page Template
                    var pageTemplate = '';
                    pageTemplate += '<div>';
                    pageTemplate += '<div class="page-item">';
                    pageTemplate += '<div class="text-container">|text|</div>';
                    pageTemplate += '<div class="book-actions">';
                    pageTemplate += '<div class="buttons">';
                    pageTemplate += '<button ng-disabled="isFirstPage()" ng-click="goToBack()" class="button button-clear button-dark icon-left ion-ios-arrow-left"></button>';
                    pageTemplate += '<button ng-disabled="isLastPage()" ng-click="goToNext()" class="button button-clear button-dark icon-right ion-ios-arrow-right"></button>';
                    pageTemplate += '</div>';
                    pageTemplate += '<a class="subdued bottom-right">{{currentPage}}</a>';
                    pageTemplate += '</div>';
                    pageTemplate += '</div>';
                    pageTemplate += '</div>';

                    // Page texts
                    var $tempElm = $('<div></div>', {
                        'class': 'temp-book-page',
                        css: {
                            height: 'auto',
                            width: $textContainer.width() + 'px',
                            'font-size': $textContainer.css('font-size'),
                            'line-height': $textContainer.css('line-height'),
                        }
                    }).hide().appendTo($('body'));
                    var pageAyetIds = [];
                    var pageNumber = 1;
                    var createPage = function(htmlStr) {
                        var pageStr = pageTemplate.replace('|text|', htmlStr);
                        $elm.turn('addPage', $compile(pageStr)(scope));
                        scope.$apply(function() {
                            scope.pageIndexes[pageNumber] = pageAyetIds;
                        });
                        pageNumber++;

                        if ($elm.turn('pages') == 2 && !$elm.data('blank-page-removed')) {
                            $elm.turn('removePage', 1);
                            $elm.data('blank-page-removed', true);
                        }

                        $tempElm.html('');
                    };
                    var textContainerHeight = $textContainer.height();
                    _.each(_.sortBy(scope.ayetler, 'ayet_index'), function(ayet, ayetLoopIndex) {
                        var currentTempHtml = $tempElm.prop('innerHTML'),
                            h = '';

                        h += '<strong>' + ayet.ayet_index + ') </strong>';
                        h += scope.markupText(ayet.ayet).replace(/\|([0-9]+)\|/g, '<span class="aciklama-position" data-sure-id="' + ayet.sure_id + '" data-ayet-id="' + ayet.id + '">$1</span>') + ' ';

                        $tempElm.append(h);

                        if ($tempElm.height() <= textContainerHeight) {
                            pageAyetIds.push(ayet.id);

                            // Check if we has finished to sure
                            if (ayetLoopIndex == scope.ayetler.length - 1) {
                                createPage($tempElm.prop('innerHTML'));

                                pageAyetIds = [];
                            }
                        } else {
                            createPage(currentTempHtml);

                            pageAyetIds = [ayet.id];

                            $tempElm.append(h);
                        }
                    });
                    //$tempElm.remove();

                    // Methods
                    scope.pageCount = $elm.turn('pages');

                    if (scope.pageCount == 1) {
                        $elm.turn("disable", true);
                    }

                    scope.isFirstPage = function() {
                        return scope.currentPage == 1;
                    };
                    scope.isLastPage = function() {
                        return scope.pageCount == scope.currentPage;
                    };
                    scope.goToBack = function() {
                        $elm.turn('previous');
                    };
                    scope.goToNext = function() {
                        $elm.turn('next');
                    };

                    if (! searchQuery) {
                        scope.$on('$stateChangeStart', function() {
                            if ($elm.turn("is")) {
                                $elm.turn('destroy');
                            }
                        });
                    }

                    // Ayet aciklamasi
                    $ionicModal.fromTemplateUrl('resources/views/quran/aciklama-modal.html', {
                        scope: scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        scope.aciklamaModal = modal;
                    });
                    $('ion-view').off('click').on('click', '.aciklama-position', function(e) {
                        e.stopPropagation();

                        var ayetPosition = parseInt($(this).text().trim());

                        Ayetler.get(parseInt($(this).data('sureId')), parseInt($(this).data('ayetId')), function(ayet) {
                            scope.$apply(function() {
                                scope.ayet = ayet;
                                scope.aciklama = _.findWhere(scope.aciklamalar, {ayet_id: ayet.id, pozisyon: ayetPosition});

                                $timeout(function() {
                                    scope.$emit('ayetLoaded');
                                });

                                scope.aciklamaModal.show();
                            });
                        });

                        return false;
                    });


                }, 250);
            });
        }
    };
});