var Meal = angular.module('meal', ['ngCordova', 'ngStorage', 'angular.filter', 'ionic']);
var db = null;

// Bootstrap angularjs
window.ionic.Platform.ready(function() {
    angular.bootstrap(document, ['meal']);
});

Meal.run(function($ionicPlatform, $cordovaSQLite, $cordovaSplashscreen, Preloader, $timeout, $http, $templateCache) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }

        // Change moment locale
        moment.locale('tr');

        // Disable sleep
        if (window.plugins.insomnia) {
            window.plugins.insomnia.keepAwake();
        }

        // Setup db connection.
        db = $cordovaSQLite.openDB({name: 'meal.db', location: 2});

        // Create tables
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS sureler (id integer primary key, ismi text, ismi_cleaned text, aciklamasi text, vidyo text, ayet_sayisi integer, sirasi integer, inis_sirasi integer)');
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS ayetler (id integer primary key, sure_id integer, ayet_index integer, ayet text, ses text)');
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS aciklamalar (id integer primary key, ayet_id integer, pozisyon integer, aciklama_index integer, aciklama text)');
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS search_pool (id integer primary key, term text, result text)');
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS notes (id integer primary key, ayet_id integer, note text, date text)');

        // Preoload views
        var views = [
            'resources/views/menu.html',
            'resources/views/quran/tabs.html',
            'resources/views/extra/meale-giris.html',
            'resources/views/extra/meale-usul.html',
            'resources/views/extra/mustafa-islamoglu.html',
            'resources/views/system/about.html',
            'resources/views/system/copyrights.html',
            'resources/views/system/settings.html',
            'resources/views/system/help.html'
        ];
        for (var i = 0; i < views.length; i++) {
            var view = views[i];
            if (! $templateCache.get(view)) {
                $http.get(view).success(function(t) {
                    $templateCache.put(view, t);
                });
            }
        }

        // Preload app images
        Preloader.Cache([
            'assets/img/about-bg.png',
            'assets/img/copyrights-bg.png',
            'assets/img/favorite-bg.png',
            'assets/img/mustafa-islamoglu.jpg',
            'assets/img/note-bg.png',
            'assets/img/paper-bg.jpg',
            'assets/img/search-bg.png'
        ]).then(function() {

            $cordovaSplashscreen.hide();

        }, function(failed) {
            console.log("An image failed: " + failed);
        });
    });
});

// Ionic configs
Meal.config(function($ionicConfigProvider, $logProvider, $compileProvider) {
    $ionicConfigProvider.backButton.text('Geri');
    $ionicConfigProvider.views.swipeBackEnabled(false);
    //$ionicConfigProvider.scrolling.jsScrolling(true);
    $ionicConfigProvider.tabs.style('standart');
    $ionicConfigProvider.tabs.position('bottom');
    //$ionicConfigProvider.views.maxCache(1);

    $logProvider.debugEnabled(false);
    $compileProvider.debugInfoEnabled(false);
});

//Meal.constant('urls', {
//    BASE: 'http://mealapi.dev',
//    BASE_API: 'http://mealapi.dev/api/v1'
//});
Meal.constant('urls', {
    BASE: 'http://mealapi.kodpage.com',
    BASE_API: 'http://mealapi.kodpage.com/api/v1'
});

// Token interceptor
Meal.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
        return {
            'request': function(config) {
                config.headers = config.headers || {};

                config.headers.api_token = window.btoa('mealapi-user');

                return config;
            },

            'responseError': function(response) {
                if (response.status === 401 || response.status === 403) {
                    //$location.path('/signin');
                }

                return $q.reject(response);
            }
        };
    });
});