Meal.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'resources/views/menu.html',
        controller: 'AppCtrl'
    });

    // Quran
    $stateProvider.state('app.quran', {
        url: '/quran',
        abstract: true,
        views: {
            'menu-content': {
                templateUrl: 'resources/views/quran/tabs.html'
            }
        }
    });
    $stateProvider.state('app.quran.sureler', {
        url: '/sureler',
        cache: false,
        views: {
            'quran-sureler': {
                templateUrl: 'resources/views/quran/sureler.html',
                controller: 'QuranSurelerCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.ayetler-slide', {
        url: '/ayetler-slide/:sure_id',
        cache: false,
        views: {
            'quran-sureler': {
                templateUrl: 'resources/views/quran/ayetler-slide.html',
                controller: 'QuranAyetlerSlideCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.ayetler-book', {
        url: '/ayetler-book/:sure_id',
        cache: false,
        views: {
            'quran-sureler': {
                templateUrl: 'resources/views/quran/ayetler-book.html',
                controller: 'QuranAyetlerBookCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.ayet', {
        url: '/ayet/:sure_id/:ayet_id',
        cache: false,
        views: {
            'quran-sureler': {
                templateUrl: 'resources/views/quran/ayet.html',
                controller: 'QuranAyetCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.search', {
        url: '/search',
        cache: false,
        views: {
            'quran-search': {
                templateUrl: 'resources/views/quran/search.html',
                controller: 'QuranSearchCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.notes', {
        url: '/notes',
        cache: false,
        views: {
            'quran-notes': {
                templateUrl: 'resources/views/quran/notes.html',
                controller: 'QuranNotesCtrl'
            }
        }
    });
    $stateProvider.state('app.quran.favorites', {
        url: '/favorites',
        cache: false,
        views: {
            'quran-favorites': {
                templateUrl: 'resources/views/quran/favorites.html',
                controller: 'QuranFavoritesCtrl'
            }
        }
    });

    // Extra
    $stateProvider.state('app.meale-giris', {
        url: '/meale-giris',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/extra/meale-giris.html'
            }
        }
    });
    $stateProvider.state('app.meale-usul', {
        url: '/meale-usul',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/extra/meale-usul.html'
            }
        }
    });
    $stateProvider.state('app.mustafa-islamoglu', {
        url: '/mustafa-islamoglu',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/extra/mustafa-islamoglu.html'
            }
        }
    });

    // System
    $stateProvider.state('app.settings', {
        url: '/settings',
        cache: false,
        views: {
            'menu-content': {
                templateUrl: 'resources/views/system/settings.html',
                controller: 'SettingsCtrl'
            }
        }
    });
    $stateProvider.state('app.help', {
        url: '/help',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/system/help.html'
            }
        }
    });
    $stateProvider.state('app.about', {
        url: '/about',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/system/about.html'
            }
        }
    });
    $stateProvider.state('app.copyrights', {
        url: '/copyrights',
        views: {
            'menu-content': {
                templateUrl: 'resources/views/system/copyrights.html'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/quran/sureler');
});