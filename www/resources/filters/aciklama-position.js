Meal.filter('aciklamaPosition', function () {
    return function(ayetText, ayet){
        ayetText = ayetText.replace(/\|([0-9]+)\|/g, '<span class="aciklama-position">$1</span>');

        return ayetText;
    }
});